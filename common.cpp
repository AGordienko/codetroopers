#include "common.h"

#ifdef DUMPTOLOG
	extern const bool DumpLog = true;
#else
	extern const bool DumpLog = false;
#endif

int PointToIndex( int x, int y, int stance )
{
	return ( x << 8 ) | ( y << 2 ) | stance;
}

void IndexToPoint( int index, int& x, int& y, int& stance ) 
{
	x = index >> 8;
	y = (index >> 2 ) & (( 1 << 6 ) - 1 );
	stance = index & ( ( 1 << 2 ) - 1 );
}

