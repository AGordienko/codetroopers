name="MyStrategy"

if [ ! -f $name.cpp ]
then
    echo Unable to find $name.cpp 
    exit 1
fi

rm -f $name

files=""

for i in *.cpp
do
    files="$files $i"
done

for i in model/*.cpp
do
    files="$files $i"
done

for i in csimplesocket/*.cpp
do
    files="$files $i"
done

g++ -static -fno-optimize-sibling-calls -fno-strict-aliasing -DONLINE_JUDGE -D_LINUX -DDUMPTOLOG -lm -s -x c++ -O2 -o $name $files 
