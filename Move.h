#pragma once
#ifndef MOVE_H
#define MOVE_H

#include "common.h"
#include "Types.h"

class CTrooperVariant;

class CTrooperVariant;

class CMove {
public:
	TMoveType MoveType;
	int X;
	int Y;

	CMove( TMoveType moveType = MT_Skip, int x = -1, int y = -1 );		 
	CMove( const CMove& other );
	bool Apply( CTrooperVariant& trooperVariant ) const;
	void MakeMove( Move& gameMove ) const;
	
	void Log() const;
		
	static int ActionPointsToMove( TTrooperType trooperType, TMoveType moveType, int stand ) { return moveType == MT_Move ? neededActionPointsForMove[stand] : neededActionPoints[trooperType][moveType]; }
	static void GetAllMoves( vector<CMove>& moves );
	static void CalculateeNeededActionPoints();

private:
	static vector< vector< int> > neededActionPoints;
	static vector<int> neededActionPointsForMove;
};


#endif //MOVE_H
