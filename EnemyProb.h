#pragma once
#ifndef ENEMYPROB_H
#define ENEMYPROB_H

#include "common.h"
#include "TrooperVariant.h"

enum TEnemyProbType {
	EPT_Visible,
	EPT_FromDamage,
	EPT_Unknown
};

class CEnemyProb {
public:
	CEnemyProb() {}
	
	void Initialize();

	void Update();
	void UpdateByVisibleEnemies();
	void UpdateByDamage();
	void UpdateVisibleCells();

	int GetEnemyProbCount( int enemyIndex, int x, int y, int stance ) const { return enemiesProbCount[enemyIndex].count( PointToIndex( x, y, stance ) ); }
	int GetEnemyProbCount( int enemyIndex, int index ) const { return enemiesProbCount[enemyIndex].count( index ); }
	int GetEnemyProbSum( int enemyIndex ) const { return enemiesProbCount[enemyIndex].size(); }
	int Size() const { return enemiesProbCount.size(); }	
	const set<int>& GetProbs( int enemyIndex ) const { return enemiesProbCount[enemyIndex]; }
	
	void SetEnemiesCenter( int teamId, int x, int y );
	bool HasEnemies() const { return hasEnemies; }
	
	TEnemyProbType GetEnemyProbType( int enemyIndex ) const { return enemyTypes[enemyIndex]; }

private:
	vector< set<int> > enemiesProbCount;
	vector<int> lastUpdatedTurn;
	vector<TEnemyProbType> enemyTypes;
	bool hasEnemies;
	vector<pair<int, int> > enemiesCenters;
	vector<int> lastTurnNumberForEnemiesCenter;
	int countOfEnemies;
	int teamSize;		
	vector<int> enemiesPoints;
	
	vector<TTrooperType> variantsEnemyType;
	vector<int> variantsMinShoot;
	vector<bool> variantsGrenade;
	vector<int> variantsStanceMask;
		
	void clearProb( int enemyIndex );
	void recalculateEnemiesCenter();
	void addInvisibleEnemiesNearCenter();
	void clearOldProbs();
	void clearProbsForUnknownEnemies( int teamId );

	TTrooperType getTypeByIndex( int index ) const { return static_cast<TTrooperType>( index % TT_Count ); }
	long long selectEnemyIdByDamage( TTrooperType myTrooperType ) const;
	void updateEnemiesPoints();
	bool buildVariantsEnemyDamage( TTrooperType damagedType, long long enemyId, bool onlyOneTrooper, bool isOrderChanged );

	void dumpLog();
};

#endif //ENEMYPROB_H	
