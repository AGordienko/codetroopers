#pragma once
#ifndef TYPES_H
#define TYPES_H

enum TTrooperType {
	TT_Medic,
	TT_Soldier,
	TT_Commander,
	TT_Sniper,
	TT_Scout,

	TT_Count
};

enum TMoveType {
	MT_Move,
	MT_Shoot,
	MT_Stand,
	MT_Skip,
	MT_UseMedikt,
	MT_UseGrenade,
	MT_UseRation,
	MT_Health,
	MT_GetRadarInfo,

	MT_Count
};

#endif //TYPES_H
