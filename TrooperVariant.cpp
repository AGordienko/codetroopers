#include "TrooperVariant.h"
#include "CurrentState.h"

vector<vector<int> > CTrooperVariant::shootDamage;
vector<vector<int> > CTrooperVariant::shootRange;
vector<int> CTrooperVariant::visibleRange;

void CTrooperVariant::CalculateShootDamage()
{
	shootDamage = vector<vector<int> >( TT_Count );
	for( int i = 0; i < TT_Count; i++ ) {
		switch( i ) {
			case TT_Commander :
				shootDamage[i].push_back(25);
				shootDamage[i].push_back(20);
				shootDamage[i].push_back(15);
				break;
			case TT_Medic :
				shootDamage[i].push_back(15);
				shootDamage[i].push_back(12);
				shootDamage[i].push_back(9);
				break;
			case TT_Soldier :
				shootDamage[i].push_back(35);
				shootDamage[i].push_back(30);
				shootDamage[i].push_back(25);
				break;
			case TT_Sniper :
				shootDamage[i].push_back(95);
				shootDamage[i].push_back(80);
				shootDamage[i].push_back(65);
				break;
			case TT_Scout :
				shootDamage[i].push_back(30);
				shootDamage[i].push_back(25);
				shootDamage[i].push_back(20);
				break;
		}
	}
}

void CTrooperVariant::CalculateShootRange()
{
	shootRange = vector<vector<int> >( TT_Count );
	for( int i = 0; i < TT_Count; i++ ) {
		switch( i ) {
			case TT_Commander :
				shootRange[i].push_back(7);
				shootRange[i].push_back(7);
				shootRange[i].push_back(7);
				break;
			case TT_Medic :
				shootRange[i].push_back(5);
				shootRange[i].push_back(5);
				shootRange[i].push_back(5);
				break;
			case TT_Soldier :
				shootRange[i].push_back(8);
				shootRange[i].push_back(8);
				shootRange[i].push_back(8);
				break;
			case TT_Sniper :
				shootRange[i].push_back(12);
				shootRange[i].push_back(11);
				shootRange[i].push_back(10);
				break;
			case TT_Scout :
				shootRange[i].push_back(6);
				shootRange[i].push_back(6);
				shootRange[i].push_back(6);
				break;
			default :
				assert( false );
		}
	}
}

void CTrooperVariant::CalculateVisibleRange()
{
	visibleRange = vector<int>( TT_Count, 7 );
	visibleRange[TT_Commander] = 8;
	visibleRange[TT_Scout] = 9;
}

CTrooperVariant::CTrooperVariant( const Trooper& trooper )
{
	X = trooper.getX();
	Y = trooper.getY();
	ActionPoints = trooper.getActionPoints();
	HasMedikt = trooper.isHoldingMedikit();
	HasGrenade = trooper.isHoldingGrenade();
	HasRation = trooper.isHoldingFieldRation();
	Health = trooper.getHitpoints();
	PlayerId = trooper.getPlayerId();
	ActualTurnNumber = -1;
	ActualSubTurnNumber = -1;
	ScoutingBonus = 0;
	TryRandomShoot = false;
	switch( trooper.getStance() ) {
		case PRONE :
			Stance = 0;
			break;
		case KNEELING :
			Stance = 1;
			break;
		case STANDING :
			Stance = 2;
			break;
	}
	
	switch( trooper.getType() ) {
		case COMMANDER :
			Type = TT_Commander;
			break;
		case FIELD_MEDIC :
			Type = TT_Medic;
			break;
		case SOLDIER :
			Type = TT_Soldier;
			break;
		case SNIPER :
			Type = TT_Sniper;
			break;
		case SCOUT :
			Type = TT_Scout;
			break;
	}
}

CTrooperVariant::CTrooperVariant( const CTrooperVariant& other )
{
	*this = other;
}

CTrooperVariant& CTrooperVariant::operator=( const CTrooperVariant& other )
{
	X = other.X;
	Y = other.Y;
	ActionPoints = other.ActionPoints;
	HasMedikt = other.HasMedikt;
	HasGrenade = other.HasGrenade;
	HasRation = other.HasRation;
	Health = other.Health;
	Stance = other.Stance;
	PlayerId = other.PlayerId;
	ActualTurnNumber = other.ActualTurnNumber;
	ActualSubTurnNumber = other.ActualSubTurnNumber;
	Type = other.Type;
	EnemiesHealth = other.EnemiesHealth;
	FriendsHealth = other.FriendsHealth;
	MovesLog = other.MovesLog;
	ScoutingBonus = other.ScoutingBonus;
	//VisibleCells = other.VisibleCells;
	TryRandomShoot = other.TryRandomShoot;

	return *this;
}

void CTrooperVariant::AddTroopersHealth()
{
	const vector<CTrooperVariant>& friends = GetCurrentState().GetMyTroopers();
	FriendsHealth = vector<int>( friends.size(), 0 );
	for( int i = 0; i < friends.size(); i++ ) {
		FriendsHealth[i] = friends[i].Health;
	}
	
	const vector<CTrooperVariant>& enemies = GetCurrentState().GetEnemyTroopers();
	EnemiesHealth.resize( enemies.size() );
	for( int i = 0; i < enemies.size(); i++ ) {
		EnemiesHealth[i] = enemies[i].Health;
	}
}

long long CTrooperVariant::GetHash() const
{
	long long res = 0LL;
	res ^= X;
	res ^= (Y << 7);
	res ^= HasMedikt << 14;
	res ^= HasGrenade << 15;
	res ^= HasRation << 16;
	res ^= Health << 17;
	res ^= Stance << 26;
	
	long long friendsHealth = 0LL;
	for( int i = 0; i < FriendsHealth.size(); i++ ) {
		friendsHealth = friendsHealth << 2;
		friendsHealth ^= FriendsHealth[i];
	}
	res ^= ( friendsHealth << 28 );

	long long enemiesHealth = 0LL;
	for( int i = 0; i < EnemiesHealth.size(); i++ ) {
		enemiesHealth = enemiesHealth << 2;
		enemiesHealth ^= EnemiesHealth[i];
	}
	res ^= ( enemiesHealth << 40 );
	
	long long usedBonusesSize = usedBonuses.size();
	res ^= ( usedBonusesSize << 58 );
	
/*
	long long  visibleCellsHash = 0;
	int shift = 0;
	for( set<int>::const_iterator it = VisibleCells.begin(); it != VisibleCells.end(); it++ ) {
		visibleCellsHash ^= (*it << shift );
		shift = (shift + 1) % 15;
	}
	res ^= visibleCellsHash << 15;
*/	
	res ^= static_cast<long long>( ScoutingBonus ) << 40;	
		
	if( TryRandomShoot ) {
		res ^= (1LL << 62 );
	}
	return res;
}

void CTrooperVariant::Log() const 
{
	cout << "Trooper " << GetTrooperTypeString( Type ) << "(" << X << ", " << Y << ")" << " L" << Health << " S" << Stance;
}

string CTrooperVariant::GetTrooperTypeString( TTrooperType type )
{
	string result;
	switch( type ) {
		case TT_Commander :
			result = "Com";
			break;
		case TT_Soldier :
			result = "Sol";
			break;
		case TT_Medic :
			result = "Med";
			break;
		case TT_Sniper :
			result = "Sn";
			break;
		case TT_Scout :
			result = "Sc";
			break;
	}
	return result;
}
