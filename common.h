#include <stdlib.h>
#include <set>
#include <vector>
#include <map>
#include <iostream>

#include "Strategy.h"
#include <assert.h>

using namespace model;
using namespace std;

extern const bool DumpLog;

int PointToIndex( int x, int y, int stance );
void IndexToPoint( int index, int& x, int& y, int& stance ); 
