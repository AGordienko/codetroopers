#include "FightTactic.h"
#include "CurrentState.h"

static int getDamageFromEnemy( TTrooperType trooperType, int stance, int usedMoves )
{
	const int actionPointsPerShoot = CMove::ActionPointsToMove( trooperType, MT_Shoot, 2 );
	const int actionPointsCount = 12; //trooperType == TT_Scout ? 12 : 10;
	const int shootDamage = CTrooperVariant::GetShootDamage( trooperType, stance ); 
	const int totalDamage = max( 0, ( (actionPointsCount - usedMoves ) / actionPointsPerShoot ) * shootDamage );
	return totalDamage;
}

CFightTactic::~CFightTactic()
{
	for( int i = 0; i < minMovesBuffer.size(); i++ ) {
		delete minMovesBuffer[i];
	}
}

bool CFightTactic::IsMoveUseless( const CMove& move ) const
{
	switch( move.MoveType ) {
		case MT_Skip :
		case MT_GetRadarInfo :
			return true;
		case MT_Shoot :
			if( move.Y == -1 && GetCurrentState().GetCurrentTrooper().Type != TT_Sniper) {
				return true;
			}
			break;
	}
	return false;
}

double CFightTactic::GetPositionQuality( const CTrooperVariant& trooperVariant, bool dumpToLog, double bestQuality ) const
{
//Bonuses:
	const int BonusForStance = 7;
	const int BonusForKill = 50;
	const int BonusForMedict = 10;
	const int BonusForRation = 10;
	const int BonusForGrenade = 10;
	const int BonusForThrowGrenadePossible = 20;
	const int PenaltyForUsingMedikt = 15;
	const int PenaltyForUsingGrenade = 15;
	const int PenaltyForUsingRation = 10;
	const int PenaltyForEnemyWithGrenade = -15;
	const int PenaltyForOneMove = 7;
	const int PenaltyForVisibleMe = 8;
	
	const vector<CTrooperVariant>& myTroopers = GetCurrentState().GetMyTroopers();
	const vector<CTrooperVariant>& enemyTroopers = GetCurrentState().GetEnemyTroopers();
		
//1. My wounds:	
	int myWounds = max( 0, 100 - trooperVariant.Health ) + friendsWounds;
	for( int i = 0; i < myTroopers.size(); i++ ) {
		myWounds += max( 0, 100 - trooperVariant.FriendsHealth[i] );
	}
	
//2. Enemy wounds and numberOfKills:
	double enemyWounds = 0;
	int numberOfKills = 0;
	int minHealth = 200;
	for( int i = 0; i < enemyTroopers.size(); i++ ) {
		enemyWounds += min( enemyTroopers[i].Health, enemyTroopers[i].Health - trooperVariant.EnemiesHealth[i] );
		if( trooperVariant.EnemiesHealth[i] <= 0 ) {
			numberOfKills++;
		} else {
			minHealth = min( minHealth, trooperVariant.EnemiesHealth[i] );
		}
	}

// Can shoot to smth:
	bool isInFight = false;
	for( int i = 0; i < enemyTroopers.size(); i++ ) {
		isInFight = isInFight || ( trooperVariant.EnemiesHealth[i] > 0 && GetCurrentState().CanShootToEnemy( trooperVariant, enemyTroopers[i] ) );
	}

// 3. Stance bonus:
	int stanceBonus = 0;
	if( isInFight ) {
		stanceBonus = ( 2 - trooperVariant.Stance ) * BonusForStance;
	} else if( trooperVariant.Type != TT_Sniper ) {
//		stanceBonus = -( 2 - trooperVariant.Stance ) * BonusForStance;
	}
	
// 4. Items bonus:
	int itemsBonus = 0;
	itemsBonus += trooperVariant.HasMedikt ? BonusForMedict : 0;
	itemsBonus += trooperVariant.HasGrenade ? BonusForGrenade : 0;
	itemsBonus += trooperVariant.HasRation ? BonusForRation : 0;
	
// 5. Grenade bonus and penalty:
	int grenadeBonus = 0;
	bool isEnemyNearby = false;
	int countOfEnemiesWithGrenades = 0;
	for( int i = 0; i < enemyTroopers.size(); i++ ) {
		if( trooperVariant.EnemiesHealth[i] <= 0 ) {
			continue;
		}
		const bool isNearby = GetCurrentState().GetDistanceEuk( trooperVariant.X, trooperVariant.Y, enemyTroopers[i].X, enemyTroopers[i].Y ) < 6 + 1e-7;
		isEnemyNearby = isEnemyNearby || isNearby;
		if( enemyTroopers[i].HasGrenade && isEnemyNearby ) {
			countOfEnemiesWithGrenades++;
		}
	}
	if( isEnemyNearby && trooperVariant.HasGrenade ) {
		grenadeBonus += BonusForThrowGrenadePossible;
	}

// 6. Penalty for using Items:
	int useItemsPenalty = 0;
	if( !trooperVariant.HasMedikt && GetCurrentState().GetCurrentTrooper().HasMedikt ) {
		const CTrooperVariant& oldTrooper = GetCurrentState().GetTrooperAtStartSubTurn();
		int oldWounds = max( 0, 100 - oldTrooper.Health ) + friendsWounds;
		for( int i = 0; i < myTroopers.size(); i++ ) {
			oldWounds += max( 0, 100 - oldTrooper.FriendsHealth[i] );
		}
		int delta = oldWounds - myWounds;
		if( trooperVariant.Type == TT_Medic ) {
			for( int i = 0; i < trooperVariant.MovesLog.size(); i++ ) {
				if( trooperVariant.MovesLog[i].MoveType == MT_Health ) {
					delta -= trooperVariant.MovesLog[i].Y == -1 ? 3 : 5;
				}
			}
		}
		if( delta < 30 ) {
			useItemsPenalty += PenaltyForUsingMedikt;
		}
	}
	if( !trooperVariant.HasGrenade && GetCurrentState().GetCurrentTrooper().HasGrenade ) {
		useItemsPenalty += PenaltyForUsingGrenade;
	}
	if( !trooperVariant.HasRation && GetCurrentState().GetCurrentTrooper().HasRation ) {
		useItemsPenalty += PenaltyForUsingRation;
	}
	
// 7. Distance to Medic:
	int medicPenalty = 0;
	if( trooperVariant.Type == TT_Medic ) {
		int bestDist = NonReachable;
		for( int i = 0; i < myTroopers.size(); i++ ) {
			if( !isTrooperInFight[i] ) {
				continue;
			}
			const int dist = GetCurrentState().GetDistance( trooperVariant.X,
					trooperVariant.Y, myTroopers[i].X, myTroopers[i].Y );
			bestDist = min( dist - 1, bestDist );
		}
		if( bestDist == NonReachable ) {
			bestDist = 0;
		}
		medicPenalty += bestDist * PenaltyForOneMove;
	} else if( medicIndex != -1 && isInFight ) {
		const int dist = GetCurrentState().GetDistance( trooperVariant.X, trooperVariant.Y,
			myTroopers[medicIndex].X, myTroopers[medicIndex].Y );
		medicPenalty += ( dist - 1 ) * 1;
	}

// Position penalty:
	//int positionPenaltyNew = GetCurrentState().GameType() == GT_Five ? getPositionPenaltyNew( trooperVariant ) : getPositionPenalty( trooperVariant );
	int positionPenaltyNew = getPositionPenaltyNew( trooperVariant );

	//Compare number of moves from myPosition to shooting position and from enemy position to shooting position 
	int bonusForShelter = 0;
	if( DumpLog ) {
		cout << "ShelterBonus:" << "[";
	}
	for( int i = 0; i < enemyTroopers.size(); i++ ) {
		if( trooperVariant.EnemiesHealth[i]  <= 0 ) {
			continue;
		}
		const int movesToShootFromEnemy = movesToShootFromEnemies[i].Get( trooperVariant.X, trooperVariant.Y, trooperVariant.Stance );
		const int movesToShootToEnemy = movesToShootToEnemies[i].Get( trooperVariant.X, trooperVariant.Y, trooperVariant.Stance );
		if( DumpLog ) {
			cout << "(E:" << movesToShootFromEnemy << ", M:" << movesToShootToEnemy << ") ";
		}
		if( movesToShootToEnemy < min( 4, movesToShootFromEnemy ) ) {
			//Good for me.
			bonusForShelter += PenaltyForOneMove * ( min( 3, movesToShootFromEnemy ) - movesToShootToEnemy );
		} else if( movesToShootToEnemy > movesToShootFromEnemy ) {
			bonusForShelter += -PenaltyForOneMove * ( min( 3, movesToShootToEnemy ) - movesToShootFromEnemy );
		}
		if( DumpLog ) {
			cout << bonusForShelter << ", ";
		}
	}
	if( DumpLog ) {
		cout << "]" << endl;
	}
	
	//out of battle penalty
	int outOfBattlePenalty = 0;
	if( !isInFight && enemyWounds == 0 ) {
		int bestDist = NonReachable;
		for( int i = 0; i < enemyTroopers.size(); i++ ) {
			if( trooperVariant.EnemiesHealth[i] <= 0 ) {
				continue;
			}
			bestDist = min( bestDist, movesToShootToEnemies[i].Get( trooperVariant.X, trooperVariant.Y, trooperVariant.Stance ) );
			if( trooperVariant.HasGrenade && GetCurrentState().GetDistanceEuk( trooperVariant.X, trooperVariant.Y, enemyTroopers[i].X, enemyTroopers[i].Y ) < 6 + 1e-7 ) {
				bestDist = 0;
			}
		}
		outOfBattlePenalty += min( 1000, bestDist * PenaltyForOneMove );
		if( trooperVariant.X == GetCurrentState().GetTrooperAtStartSubTurn().X && trooperVariant.Y == GetCurrentState().GetTrooperAtStartSubTurn().Y && trooperVariant.Type != TT_Medic ) {
//			outOfBattlePenalty += 10;
		}
	}
	
	vector<const set<int>*> visibleSet;
	vector<const set<int>*> visibleSetSniper;
	buildVisibleSet( visibleSet, visibleSetSniper, trooperVariant, 6 );

	double specialTrooperBonus = getSpecialTrooperBonus( trooperVariant );

	//Random shoot:
	if( trooperVariant.TryRandomShoot ) {
		CTrooperVariant testVariant = GetCurrentState().GetTrooperAtStartSubTurn();
		for( int i = 0; i < trooperVariant.MovesLog.size(); i++ ) {
			if( trooperVariant.MovesLog[i].MoveType == MT_Shoot && trooperVariant.MovesLog[i].Y == -1 ) {
				break;
			}
			trooperVariant.MovesLog[i].Apply( testVariant );
		}
		int bestPoint = 0;
		double bestQuality = 0;
		CTrooperVariant testEnemy;
		testEnemy.Type = TT_Commander;
		for( map<int, double>::const_iterator it = probForShoot.begin(); it != probForShoot.end(); it++ ) {
			IndexToPoint( it->first, testEnemy.X, testEnemy.Y, testEnemy.Stance );
			if( !GetCurrentState().CanShootToEnemy( testVariant, testEnemy ) ) {
				continue;
			}
			double quality = min( 1., it->second ) * testVariant.GetShootDamage();
			if( bestQuality < quality ) {
				bestQuality = quality;
				bestPoint = it->first;
			}
		}
//		cerr << "TRYSHOOT: " << bestQuality << endl;
		if( bestQuality > 10 ) {
			enemyWounds += 3 * bestQuality;
		} else {
			specialTrooperBonus -= 1000;
		}
		for( int i = 0; i < trooperVariant.MovesLog.size(); i++ ) {
			if( trooperVariant.MovesLog[i].MoveType == MT_Shoot && trooperVariant.MovesLog[i].Y == -1 ) {
				assert( bestPoint != -1 );
				trooperVariant.MovesLog[i].X = bestPoint;
			}
		}
	}

	double result = -myWounds + enemyWounds + numberOfKills * BonusForKill + stanceBonus + itemsBonus + grenadeBonus + bonusForShelter - positionPenaltyNew - medicPenalty  +
		(1 - minHealth / 1000.) - outOfBattlePenalty - useItemsPenalty  + specialTrooperBonus;

	if( result < bestQuality - 20 ) {
		return TacticWorstQuality;
	}
	double explorationBonus = getExplorationBonus( trooperVariant );
	result += explorationBonus;
	if( result < bestQuality ) {
		return TacticWorstQuality;
	}
	double invisibleEnemiesPenalty = getPenaltyForInvisibleEnemies( trooperVariant, visibleSet, visibleSetSniper );
	result -= invisibleEnemiesPenalty;

	if( DumpLog ) {	
		if( dumpToLog ) {
			for( int i = 0; i < trooperVariant.MovesLog.size(); i++ ) {
				trooperVariant.MovesLog[i].Log();
				cout << ' ';
			}
			cout << "quality=" << result << "; woundsToEnemies=" << enemyWounds << 
				", myWounds=" << myWounds << ", numberOfKills=" << numberOfKills <<
				", stanceBonus=" << stanceBonus << ", itemsBonus=" << itemsBonus <<
				", grenadeBonus=" << grenadeBonus << ", medicPenalty=" << medicPenalty <<
				", bonusForShelter=" << bonusForShelter << 
				", positionPenalty=" << positionPenaltyNew << ", outOfBattlePenalty=" << outOfBattlePenalty <<
				", useItemsPenalty=" << useItemsPenalty << ", invisibleEnemiesPenalty=" << invisibleEnemiesPenalty <<
				", explorationBonus=" << explorationBonus << ", specialTrooperBonus=" << specialTrooperBonus << endl;
		}
	}
	return result;
}

int CFightTactic::getPositionPenaltyNew( const CTrooperVariant& trooperVariant ) const
{
	int result = 0;

	const vector<CTrooperVariant>& enemyTroopers = GetCurrentState().GetEnemyTroopers();


	for( int enemyIdIndex = 0; enemyIdIndex < enemiesId.size(); enemyIdIndex++ ) {
		bool isVisibleForEnemy = false;
		for( int i = 0; i < enemyTroopers.size(); i++ ) {
			if( trooperVariant.EnemiesHealth[i]  <= 0 ) {
				continue;
			}
			isVisibleForEnemy = isVisibleForEnemy || GetCurrentState().IsEnemyVisible( enemyTroopers[i], trooperVariant );
		}
		if( isVisibleForEnemy ) {
			result += 20;
		}

		int minMovesToSeeMe = NonReachable;
		minMovesToSniperSeeMe = NonReachable;
		bool wasSniper = false;

		for( int i = 0; i < enemiesMoveOrder[enemyIdIndex].size(); i++ ) {
			const TTrooperType enemyType = static_cast<TTrooperType>( enemiesMoveOrder[enemyIdIndex][i] );
			const int enemyIndex = enemiesIndexes[enemyIdIndex][enemyType];
			if( enemyIndex == -1 || trooperVariant.EnemiesHealth[enemyIndex] <= 0 ) {
				continue;
			}
			const CTrooperVariant& enemy = enemyTroopers[enemyIndex];
			assert( enemy.PlayerId == enemiesId[enemyIdIndex] );
			const int movesToSeeMe = movesToSeeMeFromEnemies[enemyIndex].GetMinMovesToSeeTrooper( enemy.Type, trooperVariant.Type, trooperVariant.X, trooperVariant.Y, trooperVariant.Stance );
			int minMovesToShoot = movesToShootFromEnemies[enemyIndex].Get( trooperVariant.X, trooperVariant.Y, trooperVariant.Stance );
			if( minMovesToSeeMe > 3 ) {
				minMovesToShoot = max( minMovesToShoot, movesToSeeMe );
			}
			const int stance = min( 2, enemy.Stance + minMovesToShoot );
			const int damage = getDamageFromEnemy( enemy.Type, stance, 2 * minMovesToShoot );
			const int maxDamage = max( damage, maxDamageFromEnemy[i] ); 
			int damageFromGrenade = 0;

			if( enemy.HasGrenade && ( movesToSeeMe < 2 || minMovesToSeeMe < 4 ) ) {
				for( int x = max( 0, trooperVariant.X - 1 ); x <= min( width - 1, trooperVariant.X + 1 ); x++ ) {
					for( int y = max( 0, trooperVariant.Y - 1); y <= min( height - 1, trooperVariant.Y + 1 ); y++ ) {
						if( abs( x - trooperVariant.X ) + abs( y - trooperVariant.Y ) > 1 ) {
							continue;
						}
						if( GetCurrentState().GetDistanceEuk( enemy.X, enemy.Y, x, y ) < 6 + 1e-7 ) {
							damageFromGrenade = 60 + ( ( x == trooperVariant.X && y == trooperVariant.Y ) ? 20 : 0 );
						}
					}
				}
				for( map<int, int>::const_iterator it = enemyDamageFromGrenade[enemyIndex].begin(); it != enemyDamageFromGrenade[enemyIndex].end(); it++ ) {
					int testDamage = it->second;
					int x = it->first >> 8;
					int y = it->first & ( ( 1 << 8 ) - 1 );
					if( x == trooperVariant.X && y == trooperVariant.Y ) {
						testDamage += 80;
					} else if( abs( x - trooperVariant.X ) + abs( y - trooperVariant.Y ) == 1 ) {
						testDamage += 60;
					}
					damageFromGrenade = max( testDamage, damageFromGrenade );
				}
			}
			result += max( maxDamage, damageFromGrenade );

			minMovesToSeeMe = min( minMovesToSeeMe, movesToSeeMe );
			if( enemyType == TT_Sniper ) {
				wasSniper = true;
			} else if( !wasSniper ) {
				minMovesToSniperSeeMe = min( minMovesToSniperSeeMe, minMovesToSeeMe );
			}
		}
	}
	return result;
}

int CFightTactic::getPositionPenalty( const CTrooperVariant& trooperVariant ) const
{
	int positionPenaltyNew = 0;
	const vector<CTrooperVariant>& enemyTroopers = GetCurrentState().GetEnemyTroopers();
	bool isVisibleForEnemy = false;
	for( int i = 0; i < enemyTroopers.size(); i++ ) {
		if( trooperVariant.EnemiesHealth[i]  <= 0 ) {
			continue;
		}
		isVisibleForEnemy = isVisibleForEnemy || GetCurrentState().IsEnemyVisible( enemyTroopers[i], trooperVariant );
	}
	if( isVisibleForEnemy ) {
		positionPenaltyNew += 20;
	}
	for( int i = 0; i < enemyTroopers.size(); i++ ) {
		if( trooperVariant.EnemiesHealth[i] <= 0 ) {
			continue;
		}
		const int minMovesToShoot = min( minMovesToShootToFriends[i], movesToShootFromEnemies[i].Get( trooperVariant.X, trooperVariant.Y, trooperVariant.Stance ) );
		const int stance = min( 2, enemyTroopers[i].Stance + minMovesToShoot );
		const int totalDamage = getDamageFromEnemy( enemyTroopers[i].Type, stance, 2 * minMovesToShoot );

		int damageFromGrenade = 0;
		if( enemyTroopers[i].HasGrenade ) {
			for( int x = max( 0, trooperVariant.X - 1 ); x <= min( width - 1, trooperVariant.X + 1 ); x++ ) {
				for( int y = max( 0, trooperVariant.Y - 1); y <= min( height - 1, trooperVariant.Y + 1 ); y++ ) {
					if( abs( x - trooperVariant.X ) + abs( y - trooperVariant.Y ) > 1 ) {
						continue;
					}
					if( GetCurrentState().GetDistanceEuk( enemyTroopers[i].X, enemyTroopers[i].Y, x, y ) < 6 + 1e-7 ) {
						damageFromGrenade = 60 + ( ( x == trooperVariant.X && y == trooperVariant.Y ) ? 20 : 0 );
					}
				}
			}
			for( map<int, int>::const_iterator it = enemyDamageFromGrenade[i].begin(); it != enemyDamageFromGrenade[i].end(); it++ ) {
				int testDamage = it->second;
				int x = it->first >> 8;
				int y = it->first & ( ( 1 << 8 ) - 1 );
				if( x == trooperVariant.X && y == trooperVariant.Y ) {
					testDamage += 80;
				} else if( abs( x - trooperVariant.X ) + abs( y - trooperVariant.Y ) == 1 ) {
					testDamage += 60;
				}
				damageFromGrenade = max( testDamage, damageFromGrenade );
			}
		}
		positionPenaltyNew += max( totalDamage, damageFromGrenade );
	}
	
	return positionPenaltyNew;
}

double CFightTactic::getPenaltyForInvisibleEnemies( const CTrooperVariant& currentTrooper, const vector<const set<int>*>& visibleSet, const vector<const set<int>*>& visibleSetSniper ) const
{
	double result = 0;	
	CTrooperVariant shooter;
	const int myIndex = PointToIndex( currentTrooper.X, currentTrooper.Y, currentTrooper.Stance );
	const CEnemyProb& enemiesProb = GetCurrentState().EnemiesProb();
	for( int i = 0; i < enemiesProb.Size(); i++ ) {
		const TEnemyProbType enemyProbType = enemiesProb.GetEnemyProbType( i ); 
		if( enemyProbType == EPT_Visible ||	enemiesProb.GetEnemyProbSum( i ) == 0 ) {
			continue;
		}
		shooter.Type = static_cast<TTrooperType>( i % TT_Count );
		map<int, int>::const_iterator it = minMovesToShootForInvisibleEnemies[shooter.Type].find( myIndex );
		int bufferIndex = 0;
		if( it == minMovesToShootForInvisibleEnemies[shooter.Type].end() ) {
			minMovesToShootForInvisibleEnemies[shooter.Type][myIndex] = minMovesBuffer.size();
			bufferIndex = minMovesBuffer.size();
			minMovesBuffer.push_back( new CMinMovesToShoot( currentTrooper ) );
			minMovesBuffer.back()->BuildFromShooterToTrooper( 3, shooter, false );
		} else {
			bufferIndex = it->second;
		}
		double resultForEnemy = 0;
		const map<int, int>& minMoves = minMovesBuffer[bufferIndex]->GetMinMoves();
		const vector<const set<int>*>* currentVisibleSet = shooter.Type == TT_Sniper ? &visibleSetSniper : &visibleSet;
		
		int countOfPoints = 0;
		const set<int> currentProbs = enemiesProb.GetProbs( i );
		for( set<int>::const_iterator it = currentProbs.begin(); it != currentProbs.end(); it++ ) {
			if( !isVisibleSetHas( *currentVisibleSet, *it ) ) {
				countOfPoints++;
			}
		}

		for( map<int, int>::const_iterator it = minMoves.begin(); it != minMoves.end(); it++ ) {
			if( isVisibleSetHas( *currentVisibleSet, it->first ) ) {
				continue;
			}
			if( enemiesProb.GetEnemyProbCount( i, it->first ) == 1 ) {
				int x, y, stance;
				IndexToPoint( it->first, x, y, stance );
				if( ( shooter.Type == TT_Sniper) && /*( minMovesToSniperSeeMe > 3 ) &&*/ ( GetCurrentState().GetDistanceEuk( x, y, currentTrooper.X, currentTrooper.Y ) > 8.5 ) ) {
//					continue;
				}
				stance = min( 2, stance + it->second );
				resultForEnemy += getDamageFromEnemy( shooter.Type, stance, 2 * it->second );
			}
		}
		const double coefs = 1.;
		result += coefs * resultForEnemy / max( countOfPoints, 1 );
	}
	if( GetCurrentState().GameType() == GT_Five ) {
		if( GetCurrentState().TurnNumber >= 40 && GetMyScore() < GetEnemyScore() ) {
			result /= 9;
		} else if( GetCurrentState().TurnNumber >= 30 && GetMyScore() <= GetEnemyScore() ) {
			result /= 2;
		}
	}
	return result;
}


void CFightTactic::Initialize()
{
	const vector<CTrooperVariant>& myTroopers = GetCurrentState().GetMyTroopers();
	const vector<CTrooperVariant>& enemyTroopers = GetCurrentState().GetEnemyTroopers();
	minMovesToShootToFriends.clear();
	isFriendsVisible.clear();

	countOfFriendsVisibleForEnemies = 0;
	countOfFriendsThatEnemiesCanShoot = 0;
	isEnemyCanSeeMyTroopers = false;
	isEnemyCanShootToMyTroopers = false;
	commanderIndex = -1;
	for( int i = 0; i < myTroopers.size(); i++ ) {
		bool isVisibleForEnemy = false;
		bool isEnemyCanShoot = false;
		
		for( int j = 0; j < enemyTroopers.size(); j++ ) {
			isVisibleForEnemy = isVisibleForEnemy || GetCurrentState().IsEnemyVisible( enemyTroopers[j], myTroopers[i] );
			isEnemyCanShoot = isEnemyCanShoot || GetCurrentState().CanShootToEnemy( enemyTroopers[j], myTroopers[i] );			
		}
		if( isVisibleForEnemy ) {
			countOfFriendsVisibleForEnemies++;
			isEnemyCanSeeMyTroopers = true;
		}
		if( isEnemyCanShoot ) {
			countOfFriendsThatEnemiesCanShoot++;
			isEnemyCanShootToMyTroopers = true;
		}

		isFriendsVisible.push_back( isVisibleForEnemy );
		if( myTroopers[i].Type == TT_Commander ) {
			commanderIndex = i;
		}
	}
	
	isTrooperInFight.resize( myTroopers.size() );
	for( int i = 0; i < myTroopers.size(); i++ ) {
		isTrooperInFight[i] = false;
		for( int j = 0; j < enemyTroopers.size(); j++ ) {
			isTrooperInFight[i] = isTrooperInFight[i] || GetCurrentState().CanShootToEnemy( myTroopers[i], enemyTroopers[j] ) ||
				GetCurrentState().CanShootToEnemy( enemyTroopers[j], myTroopers[i] );
		}
	}

	medicIndex = -1;
	for( int i = 0; i < myTroopers.size(); i++ ) {
		if( myTroopers[i].Type == TT_Medic ) {
			medicIndex = i;
			break;
		}
	}
	
		
	const vector<Player>& players = GetCurrentState().world->getPlayers();	
	const long long myPlayerId = GetCurrentState().GetCurrentTrooper().PlayerId;
	for( int i = 0; i < players.size(); i++ ) {
		if( players[i].getId() == myPlayerId ) {
			continue;
		}
		enemiesId.push_back( players[i].getId() );
	}
	enemiesMoveOrder.resize( enemiesId.size() );
	enemiesIndexes.resize( enemiesId.size() );
	for( int enemyIndex = 0; enemyIndex < enemiesId.size(); enemyIndex++ ) {	
		enemiesMoveOrder[enemyIndex].clear();
		for( int i = 0; i < 3 + GetCurrentState().GameType(); i++ ) {
			enemiesMoveOrder[enemyIndex].push_back( i );
		}

		//buble sort:
		CTrooperVariant enemyForSort;
		enemyForSort.PlayerId = enemiesId[enemyIndex];
		for( int i = 1; i < enemiesMoveOrder[enemyIndex].size(); i++ ) {
			enemyForSort.Type = static_cast<TTrooperType>( enemiesMoveOrder[enemyIndex][i] );
			const int curDist = GetCurrentState().EnemyMoveOrder( enemyForSort );
			for( int j = i - 1; j >= 0; j-- ) {
				enemyForSort.Type = static_cast<TTrooperType>( enemiesMoveOrder[enemyIndex][j] );
				const int predDist = GetCurrentState().EnemyMoveOrder( enemyForSort );		
				if( curDist < predDist ) {
					swap( enemiesMoveOrder[enemyIndex][j + 1], enemiesMoveOrder[enemyIndex][j] );
				} else {
					break;
				}
			}
		}
		enemiesIndexes[enemyIndex] = vector<int>( enemiesMoveOrder[enemyIndex].size(), -1 );
		for( int i = 0; i < enemyTroopers.size(); i++ ) {
			if( enemyTroopers[i].PlayerId != enemiesId[enemyIndex] ) {
				continue;
			}
			enemiesIndexes[enemyIndex][enemyTroopers[i].Type] = i;
		}
	}

	//Min moves to shoot:	
	for( int i = 0; i < enemyTroopers.size(); i++ ) {
		movesToShootFromEnemies.push_back( CMinMovesToShoot( enemyTroopers[i] ) );
		movesToShootFromEnemies.back().BuildFromTrooper( 3, false );

		movesToShootToEnemies.push_back( CMinMovesToShoot( enemyTroopers[i] ) );
		movesToShootToEnemies.back().BuildFromShooterToTrooper( 8, GetCurrentState().GetCurrentTrooper(), false );

		movesToSeeMeFromEnemies.push_back( CMinMovesToShoot( enemyTroopers[i] ) );
		movesToSeeMeFromEnemies.back().BuildFromTrooper( 5, true );
	}

	for( int i = 0; i < enemyTroopers.size(); i++ ) {
		int minMoves = NonReachable;
		for( int j = 0; j < myTroopers.size(); j++ ) {
			if( !isFriendsVisible[j] ) {
				continue;
			}
			minMoves = min( minMoves, movesToShootFromEnemies[i].Get( myTroopers[j].X, myTroopers[j].Y, myTroopers[j].Stance ) );
		}
		minMovesToShootToFriends.push_back( minMoves );
	}

	//Max damage for enemy.

	maxDamageFromEnemy = vector<int>( enemiesMoveOrder[0].size(), 0 );
	for( int enemyIdIndex = 0; enemyIdIndex < enemiesId.size(); enemyIdIndex++ ) {
		vector<int> movesToSeeFriends( myTroopers.size(), NonReachable );
		for( int i = 0; i < enemiesMoveOrder[enemyIdIndex].size(); i++ ) {
			const TTrooperType enemyType = static_cast<TTrooperType>( enemiesMoveOrder[enemyIdIndex][i] );
			const int enemyIndex = enemiesIndexes[enemyIdIndex][enemyType];
			if( enemyIndex == -1 ) {
				continue;
			}
			const CTrooperVariant& enemy = enemyTroopers[enemyIndex];
			assert( enemy.PlayerId == enemiesId[enemyIdIndex] );
			for( int j = 0; j < myTroopers.size(); j++ ) {
				if( !GetCurrentState().IsEnemyMoveFirst( myTroopers[j], enemy ) ) {
					continue;
				}
				int minMovesToShoot = movesToShootFromEnemies[enemyIndex].Get( myTroopers[j].X, myTroopers[j].Y, myTroopers[j].Stance );
				int movesToSeeMe = movesToSeeMeFromEnemies[enemyIndex].GetMinMovesToSeeTrooper( enemy.Type, myTroopers[j].Type, myTroopers[j].X, myTroopers[j].Y, myTroopers[j].Stance );
				if( movesToSeeFriends[j] > 4 ) {	
					minMovesToShoot = max( minMovesToShoot, movesToSeeMe );
				}
				const int stance = min( 2, enemy.Stance + minMovesToShoot );
				const int damage = getDamageFromEnemy( enemy.Type, stance, 2 *  minMovesToShoot );
				maxDamageFromEnemy[i] = max( maxDamageFromEnemy[i], damage );
				movesToSeeFriends[j] = min( movesToSeeFriends[j], movesToSeeMe ); 
			}
		}
	}

	//Counting grenade info
	enemyDamageFromGrenade.clear();
	width = GetCurrentState().GetWidth();
	height = GetCurrentState().GetHeight();
	for( int i = 0; i < enemyTroopers.size(); i++ ) {
		enemyDamageFromGrenade.push_back( map<int, int>() );
		if( enemyTroopers[i].HasGrenade ) {
			for( int j = 0; j < myTroopers.size(); j++ ) {
				for( int x = max( 0, myTroopers[j].X - 1 ); x <= min( width - 1, myTroopers[j].X + 1 ); x++ ) {
					for( int y = max( 0, myTroopers[j].Y - 1); y <= min( height - 1, myTroopers[j].Y + 1 ); y++ ) {
						if( abs( x - myTroopers[j].X ) + abs( y - myTroopers[j].Y ) > 1 ) {
							continue;
						}
						if( GetCurrentState().GetDistanceEuk( x, y, enemyTroopers[i].X, enemyTroopers[i].Y ) < 6 + 1e-7 ) {
							int damage = 60 + ( ( x == myTroopers[j].X && y == myTroopers[j].Y ) ? 20 : 0 );
							for( int k = 0; k < myTroopers.size(); k++ ) {
								if( k == j ) {
									continue;
								}
								if( myTroopers[k].X == x && myTroopers[k].Y == y ) {
									damage += 80;
								} else if( abs( x - myTroopers[k].X ) + abs( y - myTroopers[k].Y ) == 1 ) {
									damage += 60;
								}
							}
							const int index = ( x << 8 ) | y;
							enemyDamageFromGrenade.back()[index] = damage;
						}
					}
				}
			}
		}
	}

	minMovesToShootForInvisibleEnemies.resize( TT_Count );

	calcProbForShoot();
}

void CFightTactic::calcProbForShoot()
{
	const CEnemyProb& enemyProb = GetCurrentState().EnemiesProb();
	for( int i = 0; i < enemyProb.Size(); i++ ) {
		const set<int>& probs = enemyProb.GetProbs( i );
		int count = probs.size();
		if( count == 0 ) {
			continue;
		}
		for( set<int>::const_iterator it = probs.begin(); it != probs.end(); it++ ) {
			int x, y, stance;
			IndexToPoint( *it, x, y, stance );
			const double p = 1. / count;
			for( int nStance = stance; nStance >= 0; nStance-- ) {
				const int nIndex = PointToIndex( x, y, nStance );
				if( probForShoot.find( nIndex ) == probForShoot.end() ) {
					probForShoot[nIndex] = 0;
				}
				probForShoot[nIndex] += p;
			}
		}
	}
}

void CFightTactic::buildVisibleSet( vector<const set<int>*>& result, vector<const set<int>*>& resultSniper, const CTrooperVariant& currentTrooper, int minMovesLeft ) const
{
	CTrooperVariant testVar = GetCurrentState().GetCurrentTrooper();
	result.clear();
	resultSniper.clear();
	result.push_back( &GetCurrentState().GetThisSubturnVisibleSet() );
	resultSniper.push_back( &GetCurrentState().GetThisSubturnVisibleSetSniper() );
	bool isPosChange = false;
	for( int i = 0; i <= currentTrooper.MovesLog.size() && testVar.ActionPoints >= minMovesLeft; i++ ) {
		if( isPosChange ) {
			const set<int>& addSet = GetCurrentState().GetVisibleSetForTrooper( testVar, false );
		//	result.insert( addSet.begin(), addSet.end() );
			result.push_back( &addSet );
			
			const set<int>& addSetSniper = GetCurrentState().GetVisibleSetForTrooper( testVar, true );
		//	resultSniper.insert( addSetSniper.begin(), addSetSniper.end() );
			resultSniper.push_back( &addSetSniper );
		}
		if( i >= currentTrooper.MovesLog.size() ) {
			break;
		}
		const CMove& move = currentTrooper.MovesLog[i];
		move.Apply( testVar );
		isPosChange = ( move.MoveType == MT_Move ) || ( move.MoveType == MT_Stand && move.Y > 0 );  
	} 
}

bool CFightTactic::isVisibleSetHas( const vector<const set<int>* >& visibleSet, int point )
{
	for( int i = 0; i < visibleSet.size(); i++ ) {
		if( visibleSet[i]->find( point ) != visibleSet[i]->end() ) {
			return true;
		}
	}
	return false;
}

double CFightTactic::getExplorationBonus( const CTrooperVariant& currentTrooper ) const
{
	double result = 0;
	vector< const set<int>* > visibleSet;
	vector< const set<int>* > visibleSetSniper;
	buildVisibleSet( visibleSet, visibleSetSniper, currentTrooper, 0 );
	int bonusForSeeEnemy = 2;
	const CEnemyProb& enemyProb = GetCurrentState().EnemiesProb();
	for( int i = 0; i < enemyProb.Size(); i++ ) {
		const vector< const set<int>* > *currentVisibleSet = (i % TT_Count == TT_Sniper ) ? &visibleSetSniper : &visibleSet;
		const set<int> currentProbs = enemyProb.GetProbs( i );
		if( currentProbs.size() == 0 ) {
			continue;
		}
		int count = 0;	
		for( set<int>::const_iterator it = currentProbs.begin(); it != currentProbs.end(); it++ ) {
			if( isVisibleSetHas( *currentVisibleSet, *it ) ) {
				count++;
			}
		}
		result += 1. * bonusForSeeEnemy * count / currentProbs.size();
	}
	return result;
}

double CFightTactic::getSpecialTrooperBonus( const CTrooperVariant& currentTrooper ) const
{
	double result = 0;
	const vector<CTrooperVariant>& myTroopers = GetCurrentState().GetMyTroopers();
	if( currentTrooper.Type == TT_Commander ) {
		int count = 0;
		for( int i = 0; i < myTroopers.size(); i++ ) {
			if( GetCurrentState().GetDistanceEuk( currentTrooper.X, currentTrooper.Y, myTroopers[i].X, myTroopers[i].Y ) < 5 + 1e-7 ) {
				count++;
			}
		}
		result += count * 7;
	} else if( currentTrooper.Type != TT_Scout && commanderIndex != -1 ) {
		if( GetCurrentState().GetDistanceEuk( currentTrooper.X, currentTrooper.Y, myTroopers[commanderIndex].X, myTroopers[commanderIndex].Y ) < 5 + 1e-7 ) {
			result += 7;
		}
	}

	if( currentTrooper.Type == TT_Sniper ) {
		vector<const set<int>*> visibleSet;
		vector<const set<int>*> visibleSetSniper;
		buildVisibleSet( visibleSet, visibleSetSniper, currentTrooper, 0 );
		const double bonusForShootOpp = 0.15;
		const CEnemyProb& enemyProb = GetCurrentState().EnemiesProb();
		for( int i = 0; i < enemyProb.Size(); i++ ) {
			const set<int> curProb = enemyProb.GetProbs( i );
			if( curProb.size() == 0 ) {
				continue;
			}
			CTrooperVariant enemy;
			enemy.Type = static_cast<TTrooperType>( i % TT_Count );
			int count = 0;
			int visibleCount = 0;
			const bool isEnemyVisible = enemyProb.GetEnemyProbType( i ) == EPT_Visible;
			const vector<const set<int>* >* currentVisibleSet = enemy.Type == TT_Sniper ? &visibleSetSniper : &visibleSet;
			for( set<int>::const_iterator it = curProb.begin(); it != curProb.end(); it++ ) {
				IndexToPoint( *it, enemy.X, enemy.Y, enemy.Stance );
				if( !isEnemyVisible && isVisibleSetHas( *currentVisibleSet, *it ) ) {
					visibleCount++;
				} else 	if( GetCurrentState().CanShootToEnemy( currentTrooper, enemy ) ) {
					count++;
				}
			}
			const int nonVisibleVariants = curProb.size() - visibleCount;
			result += bonusForShootOpp * currentTrooper.GetShootDamage() * count / max( 1, nonVisibleVariants );
		}
	}
	return result;
}
