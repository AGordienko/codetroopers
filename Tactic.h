#pragma once
#ifndef TACTIC_H
#define TACTIC_H

#include "common.h"
#include "Move.h"
#include "TrooperPosGenerator.h"

class CMinMovesToShoot;
const double TacticWorstQuality = -10000;
struct ITactic {
	virtual bool IsMoveUseless( const CMove& move ) const = 0;
	virtual double GetPositionQuality( const CTrooperVariant& trooperVariant, bool dumpToLog, double bestQuality = TacticWorstQuality ) const = 0;
	virtual void Initialize() = 0;

	virtual ~ITactic() {}
};

class CExplorationTactic : public ITactic {
public:
	virtual bool IsMoveUseless( const CMove& move ) const;
	virtual double GetPositionQuality( const CTrooperVariant& trooperVariants, bool dumpToLog, double bestQuality = TacticWorstQuality ) const;
	virtual void Initialize();
	
	virtual ~CExplorationTactic();

private:	
	int targetX;
	int targetY;
	
	bool isCommander;
	int distanceToTargetForCommander;
	TTrooperType curTrooperType;
	const CTrooperVariant* commander;
	const CTrooperVariant* medic;
	bool isMedicAlive;
	int startDistanceForLeader;
	long long startHashForTrooper;
	
	mutable vector< map<int, int> > minMovesToShootForInvisibleEnemies;
	mutable vector<CMinMovesToShoot*> minMovesBuffer;
	
	double getPenaltyForInvisibleEnemies( const CTrooperVariant& currentTrooper, const vector<const set<int>*>& visibleSet, const vector<const set<int>*>& visibleSetSniper ) const;
	
	void buildVisibleSet( vector<const set<int>*>& result, vector<const set<int>*>& resultSniper, const CTrooperVariant& currentTrooper, int minMovesLeft ) const;
	double getExplorationBonus( const CTrooperVariant& currentTrooper ) const;
	
	static int countOfTurnsLeftBeforePlane;
	static int predTurnNumber;
	
	static bool isVisibleSetHas( const vector<const set<int>* >& visibleSet, int point );
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class CCheckpoints {
	public:
		void Initialize();
		void GetCurTarget( int& x, int& y ) const;
		void SetNextTarget();
		void SetCustomTarget( int x, int y ) { customX = x; customY = y; }
	private:
		vector<pair<int, int> > checkpoints;
		int direction;
		int curTarget;
		int customX;
		int customY;
		int predTurn;
};

inline void CCheckpoints::GetCurTarget( int& x, int& y ) const
{
	if( customX != -1 && customY != -1 ) {
		x = customX;
		y = customY;
	} else {
		x = checkpoints[curTarget].first;
		y = checkpoints[curTarget].second;
	}
}

inline void CCheckpoints::SetNextTarget()
{
	if( customX != -1 && customY != -1 ) {
		customX = -1;
		customY = -1;
	} else {
		curTarget = (curTarget + checkpoints.size() + direction ) % checkpoints.size();
	}
}
CCheckpoints& GetCheckpoints();

#endif //TACTIC_H
