#include "MyStrategy.h"

#define _USE_MATH_DEFINES
#include <cmath>
#include <cstdlib>

#include "CurrentState.h"
#include "Move.h"
#include "TrooperPosGenerator.h"
#include <time.h>
#include <fstream>

using namespace model;
using namespace std;

MyStrategy::MyStrategy() { }

void MyStrategy::move(const Trooper& self, const World& world, const Game& game, Move& move)
{
	static double workingTime = 0;
	GetCurrentState().Update( world, game, self.getTeammateIndex() );
	

	CTrooperPosGenerator posGenerator; 

	clock_t tStart, tEnd;
	tStart = clock();
	posGenerator.FindBestVariant();
	tEnd = clock();
	double elapsed_secs = double( tEnd - tStart ) / CLOCKS_PER_SEC;
	workingTime += elapsed_secs;
	if( DumpLog ) {
		cout << "TOTAL TIME: " << workingTime << endl;
	}
	posGenerator.GetBestMove().MakeMove( move );

	CTrooperVariant curTrooper = GetCurrentState().GetCurrentTrooper();
	posGenerator.GetBestMove().Apply( curTrooper );
	for( int i = 0; i < GetCurrentState().GetEnemyTroopers().size(); i++ ) {
		if( IsEnemyVisible( GetCurrentState().GetEnemyTroopers()[i], GetCurrentState().GetMyTroopers(), curTrooper ) &&
			curTrooper.EnemiesHealth[i] <= 0 )
		{
			GetCurrentState().EnemyInfo().SetKilled( GetCurrentState().GetEnemyTroopers()[i].Type );
		}
	}

	delete GetCurrentState().CurrentTactic;
}
