#include "CurrentState.h"
#include "FightTactic.h"
#include "math.h"


map< long long, int > playerIdToNumber;

void CEnemyInfo::Initialize()
{
	const int countOfEnemies = GetCurrentState().GameType() == GT_Five ? TT_Count : 3 * TT_Count;
	for( int i = 0; i < countOfEnemies; i++ ) {
		isVisible.push_back( false );
		const TTrooperType trooperType = static_cast<TTrooperType>( i % TT_Count );
		const bool isInGame = trooperType < 3 + GetCurrentState().GameType();
		isAlive.push_back( isInGame );
	}
}

void CEnemyInfo::Update()
{
	for( int i = 0; i < isVisible.size(); i++ ) {
		isVisible[i] = false;
	}
	for( int i = 0; i < GetCurrentState().GetEnemyTroopers().size(); i++ ) {
		const CTrooperVariant& enemy = GetCurrentState().GetEnemyTroopers()[i];
		if( IsEnemyVisible( enemy ) ) {
			isVisible[GetEnemyIndex( enemy.PlayerId, enemy.Type )] = true;
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////
bool CCurrentState::isInitialized = false;

CCurrentState& GetCurrentState()
{
	static CCurrentState currentState;
	return currentState;
}

CCurrentState::CCurrentState() :
	world( 0 ),
	game( 0 ),
	PredTrooperType( TT_Count ),
	TurnNumber( -1 ),
	SubTurnNumber( -1 ),
	PredSubTurnNumber( -1 ),
	isNewTurn( true )
{
	troopersMoveOrder = vector<int>( TT_Count, -1 );
}

void CCurrentState::initialize()
{
	width = world->getWidth();
	height = world->getHeight();


	worldMap.resize( width * height );

	const vector<vector< CellType > >& cells = world->getCells();
	for( int i = 0; i < worldMap.size(); i++ )  {
		int x;
		int y;
		indexToPoint( i, x, y );
		switch( cells[x][y] ) {
			case FREE :
				worldMap[i] = CI_Free;
				break;
			case LOW_COVER :
				worldMap[i] = CI_CoverLow;
				break;
			case MEDIUM_COVER :
				worldMap[i] = CI_CoverMedium;
				break;
			case HIGH_COVER :
				worldMap[i] = CI_CoverHigh;
				break;
		}
	}

	isAlive = vector<bool>( TT_Count, false );

	countDistances();
	countDistancesEuk();

	CTrooperVariant::CalculateShootDamage();
	CMove::CalculateeNeededActionPoints();
	CTrooperVariant::CalculateShootRange();
	CTrooperVariant::CalculateVisibleRange();
	
	LastTurnShootDamage = 0;
	LastTurnScore = 0;
	myTroopersPosition.resize( TT_Count );
	
	
	damageToMyTroopers = vector<int>( 5, 0 );

	updateTroopers();		//We need make it before initialize checkpoints!
	GetCheckpoints().Initialize();
	isNextTrooperMove = true;
	
	const long long myPlayerId = currentTrooper.PlayerId;
	const vector<Player>& players = world->getPlayers();
	int playerIndex = 0;
	for( int i = 0; i < players.size(); i++ ) {
		playersOrder[players[i].getId()] = 0;
		if( players[i].getId() != myPlayerId ) {
			lastEnemyId = players[i].getId();
			playerIdToNumber[players[i].getId()] = playerIndex++;
		}
	}
	RandomShoot = -1;
}

static const int dxRange[] = { 1, 0, -1, 0 };
static const int dyRange[] = { 0, 1, 0, -1 };

void CCurrentState::countDistances()
{
	const int n = worldMap.size();
	distances = vector< vector<int> >( n, vector<int>( n, NonReachable ) );
	for( int i = 0; i < n; i++ ) {
		distances[i][i] = 0;
	}
	for( int i = 0; i < n; i++ ) {
		int x;
		int y;
		if( hasCoverAtCell( i ) ) {
			continue;
		}
		indexToPoint( i, x, y );
		for( int j = 0; j < 4; j++ ) {
			const int nx = x + dxRange[j];
			const int ny = y + dyRange[j];
			if( nx < 0 || nx >= width || ny < 0 || ny >= height ) {
				continue;
			}
			const int nindex = pointToIndex( nx, ny );
			if( !hasCoverAtCell( nindex ) ) {
				distances[i][nindex] = 1;
				distances[nindex][i] = 1;
			}
		}
	}


	for( int k = 0; k < n; k++ ) {
		for( int i = 0; i < n; i++ ) {
			for( int j = 0; j < n; j++ ) {
				distances[i][j] = min( distances[i][j], distances[i][k] + distances[k][j] );
			}
		}
	}
}

void CCurrentState::countDistancesEuk()
{
	const int n = worldMap.size();
	distancesEuk = vector< vector<double> >( n, vector<double>( n ) );
	for( int x0 = 0; x0 < width; x0++ ) {
		for( int y0 = 0; y0 < height; y0++ ) {
			for( int x1 = 0; x1 < width; x1++ ) {
				for( int y1 = 0; y1 < height; y1++ ) {
					const double dist = sqrt( 1.*(x1 - x0) * (x1 - x0 ) + (y1-y0) * (y1 - y0 ) );
					distancesEuk[pointToIndex( x0, y0 )][pointToIndex( x1, y1 )] = dist;
				}
			}
		}
	}
}

void CCurrentState::Update( const World& _world, const Game& _game, int _currentTrooperIndex )
{
	world = &_world;
	game = &_game;
	currentTrooperIndex = _currentTrooperIndex;
	const int oldTurnNumber = TurnNumber;
	TurnNumber = world->getMoveIndex();

	if( !isInitialized ) {
		initialize();
	}

	if( LastTurnShootDamage != 0 ) {
		const int currScore = getScore( currentTrooper.PlayerId );
		if( currScore == LastTurnScore ) {
			for( int i = 0; i < savedEnemyTroopers.size(); i++ ) {
				if( savedEnemyTroopers[i].Type == LastTurnShootToEnemy && savedEnemyTroopers[i].PlayerId == LastTurnShootToEnemyId ) {
					savedEnemyTroopers.erase( savedEnemyTroopers.begin() + i );
					break;
				}
			}
			changePlayerPos( LastTurnShootToEnemyId );
			if( RandomShoot != -1 && DumpLog ) {
				int x, y, stance;
				IndexToPoint( RandomShoot, x, y, stance );
				cerr << "TRY TO SHOOT: FALSE" << "( " << x << ", " << y << ")" << endl;
			}
		} else {
			if( RandomShoot != -1 && DumpLog ) {
				int x, y, stance;
				IndexToPoint( RandomShoot, x, y, stance );
				cerr << "TRY TO SHOOT: YES!!!!" << "( " << x << ", " << y << ")" << endl;
			} else {
				if( currScore - LastTurnScore != LastTurnShootDamage ) {
					enemyInfo.SetKilled( LastTurnShootToEnemy );
				}
			}
		}
	}
	RandomShoot = -1;
	LastTurnShootDamage = 0;
	LastTurnScore = getScore( currentTrooper.PlayerId );

	updateTroopers();
	updateBonuses();
	if( !isInitialized ) {
		const int troopersCount = 1 + myTroopers.size();
		switch( troopersCount ) {
			case 3:
				gameType = GT_Three;
				break;
			case 4:
				gameType = GT_Four;
				break;
			case 5:
				gameType = GT_Five;
				break;
		}
		enemyInfo.Initialize();
		enemiesProb.Initialize();
		isInitialized = true;
	}

	const TTrooperType currentTrooperType = currentTrooper.Type;
	if( TurnNumber == 0 ) {
		if( PredTrooperType != currentTrooperType ) {
			SubTurnNumber++;			//Using, that initially SubTurnNumber == -1.
			troopersMoveOrder[currentTrooperType] = SubTurnNumber;
		} else {
			SubTurnNumber = troopersMoveOrder[currentTrooperType];
		}
	} else {
		SubTurnNumber = troopersMoveOrder[currentTrooperType];
	}
	isNewTurn = TurnNumber != oldTurnNumber;
	isNextTrooperMove =  isNewTurn || ( SubTurnNumber != PredSubTurnNumber );
	processSavedEnemyTroopers();
	
	//Update health my friends and enemies:
	//We must do it after loading info from savedEnemyTroopers!
	currentTrooper.AddTroopersHealth();
	for( int i = 0; i < myTroopers.size(); i++ ) {
		myTroopers[i].AddTroopersHealth();
	}
	
	if( isNextTrooperMove ) {
		trooperAtStartSubTurn = currentTrooper;
	}
	

	enemyInfo.Update();
	enemiesProb.Update();

	 
	if( enemyTroopers.size() == 0 ) {
//	if( enemyTroopers.size() == 0  && !enemiesProb.HasEnemies() ) {
		CurrentState = CS_Still;
		CurrentTactic = new CExplorationTactic();
	} else {
		CurrentState = CS_Fight;
		CurrentTactic = new CFightTactic();
	}
	
	PredSubTurnNumber = SubTurnNumber;


	RandomShoot = -1;
	if( DumpLog ) {
		dump();
	}
}

void CCurrentState::updateTroopers()
{
	vector<int> predTurnHealth( TT_Count, 0 );
	if( TurnNumber > 0 ) {
		for( int i = 0; i < damageToMyTroopers.size(); i++ ) {
			damageToMyTroopers[i] = 0;
		}
		predTurnHealth[currentTrooper.Type] = currentTrooper.Health;
		damageToMyTroopers[currentTrooper.Type] = currentTrooper.Health;
		for( int i = 0; i < currentTrooper.FriendsHealth.size(); i++ ) {
			predTurnHealth[myTroopers[i].Type] = currentTrooper.FriendsHealth[i];
			damageToMyTroopers[myTroopers[i].Type] = currentTrooper.FriendsHealth[i];
		}
	}
	
	for( int i = 0; i < myTroopersPosition.size(); i++ ) {
		myTroopersPosition[i] = -1;
	}
	myTroopersPosition[currentTrooper.Type] = PointToIndex( currentTrooper.X, currentTrooper.Y, currentTrooper.Stance );
	for( int i = 0; i < myTroopers.size(); i++ ) {
		myTroopersPosition[myTroopers[i].Type] = PointToIndex( myTroopers[i].X, myTroopers[i].Y, myTroopers[i].Stance );
	}


	//Build troopers array:
	myTroopers.clear();
	enemyTroopers.clear();
	const vector<Trooper>& allVisibleTroopers = world->getTroopers();
	for( int i = 0; i < allVisibleTroopers.size(); i++ ) {
		if( allVisibleTroopers[i].isTeammate() ) {
			if( allVisibleTroopers[i].getTeammateIndex() == currentTrooperIndex ) {
				currentTrooper = CTrooperVariant( allVisibleTroopers[i] );
			} else {
				myTroopers.push_back( CTrooperVariant( allVisibleTroopers[i] ) );
			}
		} else {
			enemyTroopers.push_back( CTrooperVariant( allVisibleTroopers[i] ) );
			lastEnemyId = enemyTroopers.back().PlayerId;
		}
	}

	//Remove troopers from map:
	const int killTrooperMask = ~( CI_MyTrooper | CI_EnemyTrooper);
	for( int i = 0; i < worldMap.size(); i++ ) {
		worldMap[i] = static_cast<TCellInfo>( worldMap[i] & killTrooperMask );
	}

	//Add troopers (EXCEPT CURRENT) to map:
	for( int i = 0; i < myTroopers.size(); i++ ) {
		const int x = myTroopers[i].X;
		const int y = myTroopers[i].Y;
		TCellInfo& cell = worldMap[pointToIndex( x, y )];
		cell = static_cast<TCellInfo>( cell| CI_MyTrooper );
	}
	//Add visible enemy troopers to map:
	for( int i = 0; i < enemyTroopers.size(); i++ ) {
		const int x = enemyTroopers[i].X;
		const int y = enemyTroopers[i].Y;
		TCellInfo& cell = worldMap[pointToIndex( x, y )];
		cell = static_cast<TCellInfo>( cell| CI_EnemyTrooper );
	}

	//Update my alives troopers:
	for( int i = 0; i < isAlive.size(); i++ ) {
		isAlive[i] = false;
	}
	isAlive[currentTrooper.Type] = currentTrooper.Health > 0;
	damageToMyTroopers[currentTrooper.Type] = max( 0, predTurnHealth[currentTrooper.Type] - currentTrooper.Health );
	for( int i = 0; i < myTroopers.size(); i++ ) {
		isAlive[myTroopers[i].Type] = myTroopers[i].Health > 0;
		damageToMyTroopers[myTroopers[i].Type] = max( 0, predTurnHealth[myTroopers[i].Type] - myTroopers[i].Health );
	}
	
	updateVisibleSet();
	//currentTrooper.VisibleCells = thisSubturnVisibleSet;
	for( int i = 0; i < myTroopers.size(); i++ ) {
	//	myTroopers[i].VisibleCells = thisSubturnVisibleSet;
	}
}

void CCurrentState::updateBonuses()
{
	//Remove bonuses from map:
	const int killBonusMask = ~( CI_Medict | CI_Grenage | CI_Ration );
	for( int i = 0; i < worldMap.size(); i++ ) {
		worldMap[i] = static_cast<TCellInfo>( worldMap[i] & killBonusMask );
	}

	const vector<Bonus>& bonuses = world->getBonuses();
	for( int i = 0; i < bonuses.size(); i++ ) {
		const int x = bonuses[i].getX();
		const int y = bonuses[i].getY();
		const int index = pointToIndex( x, y );
		switch( bonuses[i].getType() ) {
			case GRENADE :
				worldMap[index] = static_cast<TCellInfo>( worldMap[index] | CI_Grenage );
				break;
			case MEDIKIT :
				worldMap[index] = static_cast<TCellInfo>( worldMap[index] | CI_Medict );
				break;
			case FIELD_RATION :
				worldMap[index] = static_cast<TCellInfo>( worldMap[index] | CI_Ration );
				break;
		}
	}

}

bool CCurrentState::IsEnemyVisible( const CTrooperVariant& me, const CTrooperVariant& enemy ) const
{
	const double visibleRange = me.GetVisibleRange( &enemy );
	return world->isVisible( visibleRange, me.X, me.Y, me.ConvertStance(),
		enemy.X, enemy.Y, enemy.ConvertStance() );
}

bool CCurrentState::CanShootToEnemy( const CTrooperVariant& me, const CTrooperVariant& enemy ) const
{
	const int shootRange = me.GetShootRange();
	return world->isVisible( shootRange, me.X, me.Y, me.ConvertStance(),
		enemy.X, enemy.Y, enemy.ConvertStance() );
}

void CCurrentState::processSavedEnemyTroopers()
{
	//Delete old enemy positions:
	for( int i = savedEnemyTroopers.size() - 1; i >= 0; i-- ) {
		if( TurnNumber > savedEnemyTroopers[i].ActualTurnNumber ||
			( !enemyInfo.IsAlive( GetEnemyIndex( savedEnemyTroopers[i].PlayerId, savedEnemyTroopers[i].Type ) ) ) ||
			( TurnNumber == savedEnemyTroopers[i].ActualTurnNumber && SubTurnNumber > savedEnemyTroopers[i].ActualSubTurnNumber ) )
		{
			savedEnemyTroopers.erase( savedEnemyTroopers.begin() + i );
			continue;
		}
		if( !IsCellFree( savedEnemyTroopers[i].X, savedEnemyTroopers[i].Y ) ) {
			//This cell is visible and contain actual enemy.
			savedEnemyTroopers.erase( savedEnemyTroopers.begin() + i );
			continue;
		}
		
		bool isEnemyVisible = IsEnemyVisible( currentTrooper, savedEnemyTroopers[i] );
		for( int j = 0; j < myTroopers.size(); j++ ) {
			assert( myTroopers[j].Health > 0 );
			isEnemyVisible = isEnemyVisible || IsEnemyVisible( myTroopers[j], savedEnemyTroopers[i] );
		}
		//Conflict:
		if( isEnemyVisible ) {
			int &playerOrder = playersOrder[savedEnemyTroopers[i].PlayerId];
			playerOrder = playerOrder == -1 ? 10 + 0 : 10 + -1;		//It's mean that we need change it after cycle.
			if( DumpLog ) {
				cout << "Saved enemy conflict. New order = " << playerOrder << endl;
				savedEnemyTroopers[i].Log();
				cout << endl;
			}
			savedEnemyTroopers.erase( savedEnemyTroopers.begin() + i );
			continue;
		}
	}
	for( map<long long, int>::iterator it = playersOrder.begin(); it != playersOrder.end(); it++ ) {
		if( it->second > 0 ) {
			it->second -= 10;  
			for( int i = savedEnemyTroopers.size() - 1; i >= 0; i-- ) {
				if( savedEnemyTroopers[i].PlayerId != it->first ) {
					continue;
				}
				if( it->second == -1 ) {
					savedEnemyTroopers[i].ActualSubTurnNumber--;
					if( TurnNumber == savedEnemyTroopers[i].ActualTurnNumber && SubTurnNumber > savedEnemyTroopers[i].ActualSubTurnNumber ) {
						savedEnemyTroopers.erase( savedEnemyTroopers.begin() + i );
					}
				} else {
					savedEnemyTroopers[i].ActualSubTurnNumber++;
				}
			}
		}
	}
	//Adding unvisible saved enemies:
	firstInvisibleTrooperIndex = enemyTroopers.size();
	for( int i = 0; i < savedEnemyTroopers.size(); i++ ) {
		const CTrooperVariant& enemyTrooper = savedEnemyTroopers[i];
		enemyTroopers.push_back( enemyTrooper );
		TCellInfo &cellInfo = worldMap[pointToIndex( enemyTrooper.X, enemyTrooper.Y)];
		cellInfo = static_cast<TCellInfo>( cellInfo | CI_EnemyTrooper );
	}

	if( DumpLog ) {
	}
}

void CCurrentState::SaveEnemyTroopers( const CMove& move )
{
	savedEnemyTroopers.clear();
	CTrooperVariant currentMoveCopy = currentTrooper;
	move.Apply( currentMoveCopy );
	for( int i = 0; i < enemyTroopers.size(); i++ ) {
		const int enemyHealth = currentMoveCopy.EnemiesHealth[i];
		if( enemyHealth <= 0 ) {
			if( i < firstInvisibleTrooperIndex ) {
				enemyInfo.SetKilled( GetEnemyIndex( enemyTroopers[i].PlayerId, enemyTroopers[i].Type ) );
			}
			continue;
		}
		int subTurnForEnemy = troopersMoveOrder[enemyTroopers[i].Type];
		if( subTurnForEnemy == -1 ) {
			//It is possible if TurnNumber == 0
			continue;
		}
		if( playersOrder[enemyTroopers[i].PlayerId] == -1 ) {
			subTurnForEnemy--;
		}
		int turnNumberForEnemy = subTurnForEnemy < SubTurnNumber ? TurnNumber + 1 : TurnNumber;
		savedEnemyTroopers.push_back( enemyTroopers[i] );
		//Health may changed after this turn:
		savedEnemyTroopers.back().Health = enemyHealth;
		if( i < firstInvisibleTrooperIndex ) {
			savedEnemyTroopers.back().ActualTurnNumber = turnNumberForEnemy;
			savedEnemyTroopers.back().ActualSubTurnNumber = subTurnForEnemy;
		}
	}

	//Set new checkpoint for ExplorationTactic
	int checkpointX = -1;
	int checkpointY = -1;
	int bestDistToCheckpoint = NonReachable;
	for( int i = 0; i < enemyTroopers.size(); i++ ) {
		const int dist = GetDistance( currentTrooper.X, currentTrooper.Y, enemyTroopers[i].X, enemyTroopers[i].Y );
		if( dist < bestDistToCheckpoint ) {
			bestDistToCheckpoint = dist;
			checkpointX = enemyTroopers[i].X;
			checkpointY = enemyTroopers[i].Y;
		}
	}
	if( bestDistToCheckpoint != NonReachable ) {
		GetCheckpoints().SetCustomTarget( checkpointX, checkpointY );
	}


}

void CCurrentState::changePlayerPos( long long playerId )
{
	int& order = playersOrder[playerId];
	order = order == -1 ? 0 : -1;
	for( int i = savedEnemyTroopers.size() - 1; i >= 0; i-- ) {
		if( savedEnemyTroopers[i].PlayerId != playerId ) {
			continue;
		}
		if( order == -1 ) {
			savedEnemyTroopers[i].ActualSubTurnNumber--;
		} else {
			savedEnemyTroopers[i].ActualSubTurnNumber++;
		}
	}
	if( DumpLog ) {
		cout << "CHANGE PLAYER POS: " << order << endl;
	}
}

void CCurrentState::addVisiblesCell( set<int>& result, const CTrooperVariant& trooper, bool isEnemySniper ) const
{
	CTrooperVariant enemy;
	enemy.Type = isEnemySniper ? TT_Sniper : TT_Commander;
	const int visibleRange = trooper.GetVisibleRange();
	for( int x = max( 0, trooper.X - visibleRange ); x <= min( width - 1, trooper.X + visibleRange ); x++ ) {
		for( int y = max( 0, trooper.Y - visibleRange ); y <= min( height - 1, trooper.Y + visibleRange ); y++ ) {
			if( !IsCellWithoutCover( x, y ) ) {
				continue;
			}
			for( int stance = 2; stance >= 0; stance-- ) {
				bool isCellVisible = false;
				enemy.X = x;
				enemy.Y = y;
				enemy.Stance = stance;
				if( IsEnemyVisible( trooper, enemy ) ) {
					result.insert( PointToIndex( x, y, stance ) );
				} else {
					break;
				}
			}
		}
	}
}

void CCurrentState::updateVisibleSet()
{
	if( isNextTrooperMove ) {
		thisSubturnVisibleSet.clear();
		thisSubturnVisibleSetSniper.clear();
		for( int i = 0; i < myTroopers.size(); i++ ) {
			const set<int>& visibleSet = GetVisibleSetForTrooper( myTroopers[i], false );
			thisSubturnVisibleSet.insert( visibleSet.begin(), visibleSet.end() );
			const set<int>& visibleSetSniper = GetVisibleSetForTrooper( myTroopers[i], true );
			thisSubturnVisibleSetSniper.insert( visibleSetSniper.begin(), visibleSetSniper.end() );
		}
	}
	const set<int>& visibleSet = GetVisibleSetForTrooper( currentTrooper, false );
	thisSubturnVisibleSet.insert( visibleSet.begin(), visibleSet.end() );
	const set<int>& visibleSetSniper = GetVisibleSetForTrooper( currentTrooper, true );
	thisSubturnVisibleSetSniper.insert( visibleSetSniper.begin(), visibleSetSniper.end() );
}

int CCurrentState::EnemyMoveOrder( const CTrooperVariant& enemy )
{
	const int enemySubTurn = troopersMoveOrder[enemy.Type] + playersOrder[enemy.PlayerId]; 
	const int enemyDist = ( enemySubTurn + troopersMoveOrder.size() - SubTurnNumber ) % troopersMoveOrder.size();
	return enemyDist;
}

bool CCurrentState::IsEnemyMoveFirst( const CTrooperVariant& me, const CTrooperVariant& enemy )
{
	const int mySubTurn = troopersMoveOrder[me.Type]; 
	const int myDist = ( mySubTurn + troopersMoveOrder.size() - SubTurnNumber ) % troopersMoveOrder.size();
	const int enemySubTurn = troopersMoveOrder[enemy.Type] + playersOrder[enemy.PlayerId]; 
	const int enemyDist = ( enemySubTurn + troopersMoveOrder.size() - SubTurnNumber ) % troopersMoveOrder.size();

	return enemyDist < myDist;
}

const set<int>& CCurrentState::GetVisibleSetForTrooper( const CTrooperVariant& trooper, bool isEnemySniper )
{
	const int index = ( PointToIndex( trooper.X, trooper.Y, trooper.Stance ) << 5 ) | ( trooper.Type << 1 ) | ( isEnemySniper ? 1 : 0 );
	map<int, set<int> >::const_iterator it = savedVisibleSetForTrooper.find( index );
	if( it != savedVisibleSetForTrooper.end() ) {
		return it->second;
	}
	set<int>& result = savedVisibleSetForTrooper.insert( make_pair( index, set<int>() ) ).first->second;
	addVisiblesCell( result, trooper, isEnemySniper );
	return result;
}

void CCurrentState::dump()
{
	if( isNewTurn ) {
		cout << "NEW TURN: " << TurnNumber << endl;
	}
	if( isNextTrooperMove ) {
		cout << "Next trooper. Damage:[";
		for( int i = 0; i < damageToMyTroopers.size(); i++ ) {
			cout << " " << damageToMyTroopers[i];
		}
		cout << "]" << endl;
	}
	cout << "Update troopers: " << endl;
	cout << "Current: ";
	currentTrooper.Log();
	cout << endl;
	cout << "My: ";
	for( int i = 0; i < myTroopers.size(); i++ ) {
		myTroopers[i].Log();
	}
	cout << endl;
	cout << "Enemy: ";
	for( int i = 0; i < enemyTroopers.size(); i++ ) {
		enemyTroopers[i].Log();
	}
	cout << endl;
	if( savedEnemyTroopers.size() > 0 ) {
		cout << "Adding unvisible enemies: " << savedEnemyTroopers.size() << endl;
		for( int i = 0; i < enemyTroopers.size(); i++ ) {
			enemyTroopers[i].Log();
		}
		cout << endl;
	}
}

///////////////////////////////////////////////////////////////////////////////////
bool IsEnemyVisible( const CTrooperVariant& enemy, const vector<CTrooperVariant>& friends, const CTrooperVariant& me )
{
	bool result = GetCurrentState().IsEnemyVisible( me, enemy );
	for( int i = 0; i < friends.size(); i++ ) {
		result = result || GetCurrentState().IsEnemyVisible( friends[i], enemy );
	}
	return result;
}

int GetMyScore()
{
	return getScore( GetCurrentState().GetCurrentTrooper().PlayerId );
}

int GetEnemyScore()
{
	return getScore( getEnemyId() );
}

int getScore( long long playerId )
{
	const vector<Player>& players = GetCurrentState().world->getPlayers();
	for( int i = 0; i < players.size(); i++ ) {
		if( players[i].getId() == playerId ) {
			return players[i].getScore();
		}
	}
	assert( false );
	return 0;
}

long long getEnemyId()
{
	long long myId = GetCurrentState().GetCurrentTrooper().PlayerId;
	const vector<Player>& players = GetCurrentState().world->getPlayers();
	for( int i = 0; i < players.size(); i++ ) {
		if( players[i].getId() != myId ) {
			return players[i].getId();
		}
	}
	assert( false );
	return 0;
}

int GetEnemyIndex( long long playerId, TTrooperType type )
{
	return playerIdToNumber[playerId] * TT_Count + type;
} 

long long GetEnemyIdByIndex( int enemyIndex )
{
	int id = enemyIndex / TT_Count;
	for( map<long long, int>::const_iterator it = playerIdToNumber.begin(); it != playerIdToNumber.end(); it++ )
	{
		if( id == it->second ) {
			return it->first;
		}
	}
	assert( false );
	return 0;
}

int GetTeamIndex( long long playerId )
{
	return playerIdToNumber[playerId];
}
