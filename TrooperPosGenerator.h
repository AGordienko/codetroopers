#pragma once
#ifndef TROOPERPOSGENERATOR_H
#define TROOPERPOSGENERATOR_H

#include "common.h"
#include "Tactic.h"
#include "TrooperVariant.h"

class CTrooperPosGenerator {
public:
	CTrooperPosGenerator();

	void FindBestVariant();
	const CMove& GetBestMove() const;

private:
	CTrooperVariant bestVariant;
	CMove bestVariantMove;
	double bestVariantQuality;
};

#endif //TROOPERPOSGENERATOR_H
