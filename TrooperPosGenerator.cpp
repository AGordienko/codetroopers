#include "TrooperPosGenerator.h"
#include "CurrentState.h"
#include <set>

CTrooperPosGenerator::CTrooperPosGenerator() 
{
}

void CTrooperPosGenerator::FindBestVariant()
{
	const CTrooperVariant& startVariant = GetCurrentState().GetCurrentTrooper();
	ITactic* tactic = GetCurrentState().CurrentTactic;
	tactic->Initialize();

	bestVariant = startVariant;
	bestVariantMove = CMove();
	bestVariantQuality = tactic->GetPositionQuality( bestVariant, true );

	map<long long, int> processedVariants;
	map<long long, int>::iterator it;

	processedVariants.insert( make_pair( bestVariant.GetHash(), bestVariant.ActionPoints ) );

	vector<CMove> moves;
	CMove::GetAllMoves( moves );
	for( int i = moves.size() - 1; i >= 0; i-- ) {
		if( tactic->IsMoveUseless( moves[i] ) ) {
			moves.erase( moves.begin() + i );
		}
	}
	
	unsigned long long bestMovesMask = 0LL;
	vector<double> firstMoveQuality( moves.size(), TacticWorstQuality );

	const int maxDepth = 25;
	vector<CTrooperVariant> variantTree( maxDepth );
	vector<int> firstMoveIndex( maxDepth );
	variantTree[0] = startVariant;
	firstMoveIndex[0] = 0;
	int depth = 0;
	CTrooperVariant curVariant = startVariant;
	if( DumpLog ) {
		cout << "Start pos generator for ";
		curVariant.Log();
		cout << endl;
		for( int i = 0; i < moves.size(); i++ ) {
			moves[i].Log();
			cout << " ";
		}
		cout << "IsStartOfTurn: " << ( GetCurrentState().IsNextTrooperMove() ? "True" : "False" ) << endl;
		cout << endl;
	}
	int numberOfVariants = 0;

	while( depth < maxDepth - 1 ) {
		//		cout << depth << " " << curVariant.ActionPoints << endl;
		int moveIndex = firstMoveIndex[depth];
		CTrooperVariant predVariant = curVariant;
		for( ; moveIndex < moves.size(); moveIndex++ ) {
			if( moves[moveIndex].Apply( curVariant ) ) {
				const long long hash = curVariant.GetHash();
				it = processedVariants.find( hash );
				if( it == processedVariants.end() || it->second < curVariant.ActionPoints ) {
					processedVariants[hash] = curVariant.ActionPoints;
					break;
				} else {
					curVariant = predVariant;
					//cout << "Repeat: " << hash << " ";
				}
			}
		}
		if( moveIndex < moves.size() ) {
			curVariant.MovesLog.push_back( moves[moveIndex] );
			firstMoveIndex[depth] = moveIndex + 1;
			const bool needPreciseCalc = DumpLog || depth == 0;
			const double testQuality = tactic->GetPositionQuality( curVariant, true, needPreciseCalc ? TacticWorstQuality : bestVariantQuality );
			numberOfVariants++;
			if( testQuality > bestVariantQuality ) {
				bestVariantQuality = testQuality;
				bestVariant = curVariant;
				//				cout << "New best move. first move id: " << firstMoveIndex[0] << endl;
				bestVariantMove = moves[firstMoveIndex[0] - 1];
				bestMovesMask = 1 << ( firstMoveIndex[0] - 1 );
				//HACK!
				if( moves[moveIndex].MoveType == MT_Shoot && moves[moveIndex].Y == -1 ) {
					moves[moveIndex].X = curVariant.MovesLog.back().X;
				}
			} else if( testQuality == bestVariantQuality ) {
				bestMovesMask = bestMovesMask | ( 1 << ( firstMoveIndex[0] - 1 ) );
			}
			if( depth == 0 ) {
				firstMoveQuality[moveIndex] = testQuality;
			}
			depth++;
			variantTree[depth] = curVariant;
			firstMoveIndex[depth] = 0;
		} else {
			//			cout << "Decrease: " << endl;
			curVariant.MovesLog.pop_back();
			if( depth == 0 ) {
				break;
			}
			depth--;
			curVariant = variantTree[depth];
		}
	}
	
	double bestQualityForOneMove = TacticWorstQuality;
	for( int i = 0; i < firstMoveQuality.size(); i++ ) {
		if( bestMovesMask & (1 << i) && bestQualityForOneMove < firstMoveQuality[i] ) {
			bestQualityForOneMove = firstMoveQuality[i];
			bestVariantMove = moves[i];
		}
	}
	if( DumpLog ) {
		cout << "**************************************************" << endl;
		cout << "Number of variants: " << numberOfVariants << endl;
		tactic->GetPositionQuality( bestVariant, true );
		bestVariantMove.Log();
		cout << endl;
		cout << "**************************************************" << endl;
	}
}

const CMove& CTrooperPosGenerator::GetBestMove() const
{
	return bestVariantMove;
}
