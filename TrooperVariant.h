#pragma once
#ifndef TROOPERVARIANT_H
#define TROOPERVARIANT_H

#include "common.h"
#include "Types.h"
#include "Move.h"

class CTrooperVariant {
public:
	int X;
	int Y;
	int ActionPoints;
	bool HasMedikt;
	bool HasGrenade;
	bool HasRation;
	int Health;
	int Stance;
	long long PlayerId;
	int ActualTurnNumber;
	int ActualSubTurnNumber;
	bool TryRandomShoot;
	mutable int ScoutingBonus;
						
	TTrooperType Type;
	
	vector<int> EnemiesHealth;
	vector<int> FriendsHealth;
	mutable vector<CMove> MovesLog;
	//set<int> VisibleCells;
	
	bool IsBonusUsed( int x, int y ) const { return usedBonuses.find( x * 30 + y ) != usedBonuses.end(); }
	void AddBonusToUsed( int x, int y ) { usedBonuses.insert( x * 30 + y ); }

	CTrooperVariant() {}		
	CTrooperVariant( const Trooper& trooper );
	CTrooperVariant( const CTrooperVariant& other );
	CTrooperVariant& operator=( const CTrooperVariant& other );
			
	int GetShootDamage() const { return shootDamage[Type][Stance]; }
	int GetShootRange() const { return shootRange[Type][Stance]; }
	double GetVisibleRange( const CTrooperVariant* obj = 0 ) const;
	int GetMaxActionPoints() const;
	static void CalculateShootDamage();
	static void CalculateShootRange();
	static void CalculateVisibleRange();
	TrooperStance ConvertStance() const;
	static TrooperStance ConvertStance( int stance );
	static int GetShootDamage( TTrooperType type, int stance ) { return shootDamage[type][stance]; }
	void AddTroopersHealth();
		
	long long GetHash() const;
	
	void Log() const;
	static string GetTrooperTypeString( TTrooperType type );

private:
	set<int> usedBonuses;
	
	static vector<vector<int> > shootDamage;
	static vector<vector<int> > shootRange;
	static vector<int> visibleRange;
};

inline TrooperStance CTrooperVariant::ConvertStance() const
{
	return ConvertStance( Stance );
}

inline TrooperStance CTrooperVariant::ConvertStance( int stance )
{
	switch( stance ) {
		case 0:
			return PRONE;
		case 1:
			return KNEELING;
		case 2:
			return STANDING;
	}
	assert( false );
}

inline int CTrooperVariant::GetMaxActionPoints() const
{
	switch( Type ) {
		case TT_Scout :
			return 12;
		default :
			return 10;
	}
}

inline double CTrooperVariant::GetVisibleRange( const CTrooperVariant* obj ) const
{
	double penalty = 0;
	if( obj != 0 && obj->Type == TT_Sniper && Type != TT_Scout ) {
		if( obj->Stance == 0 ) {
			penalty = 1;
		} else if( obj->Stance == 1 ) {
			penalty = 0.5;
		}
	}
	return visibleRange[Type] - penalty;
}
#endif //TROOPERVARIANT_H
