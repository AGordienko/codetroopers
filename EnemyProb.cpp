#include "EnemyProb.h"
#include "CurrentState.h"
#include "TrooperVariant.h"
#include "MinMovesToShoot.h"


void CEnemyProb::Initialize()
{
	countOfEnemies = GetCurrentState().GameType() == GT_Five ? TT_Count : 3 * TT_Count;
	enemiesProbCount.resize( countOfEnemies );
	lastUpdatedTurn = vector<int>( countOfEnemies, 0 );
	enemyTypes = vector<TEnemyProbType>( countOfEnemies, EPT_Unknown );
	hasEnemies = false;
	teamSize = GetCurrentState().GameType() == GT_Five ? 1 : 3;
	for( int i = 0; i < teamSize; i++ ) {
		enemiesCenters.push_back( make_pair( -1, -1 ) );
		lastTurnNumberForEnemiesCenter.push_back( 0 );
		enemiesPoints.push_back( 0 );
	}
}

void CEnemyProb::Update()
{	
	clearOldProbs();
	
	UpdateByVisibleEnemies();
	recalculateEnemiesCenter();
	if( GetCurrentState().IsNextTrooperMove() ) {
		UpdateByDamage();
	}
	UpdateVisibleCells();

	addInvisibleEnemiesNearCenter();
	
	hasEnemies = false;
	for( int i = 0; i < enemiesProbCount.size(); i++ ) {
		hasEnemies = hasEnemies || enemiesProbCount[i].size() > 0;
	}
	
	updateEnemiesPoints();
	dumpLog();
}

void CEnemyProb::updateEnemiesPoints()
{
	const long long myPlayerId = GetCurrentState().GetCurrentTrooper().PlayerId;
	const vector<Player>& players = GetCurrentState().world->getPlayers();
	for( int i = 0; i < players.size(); i++ ) {
		if( players[i].getId() != myPlayerId ) {
			const int teamId = GetTeamIndex( players[i].getId() );
			const int score = players[i].getScore();
			enemiesPoints[teamId] = score;
		}
	}
}

void CEnemyProb::UpdateByVisibleEnemies()
{
	for( int i = 0; i < GetCurrentState().GetEnemyTroopers().size(); i++ ) {
		const CTrooperVariant& enemy = GetCurrentState().GetEnemyTroopers()[i];
		const int enemyIndex = GetEnemyIndex( enemy.PlayerId, enemy.Type );
		if( !GetCurrentState().EnemyInfo().IsAlive( enemyIndex ) ) {
			continue;
		}
		clearProb( enemyIndex );
		enemyTypes[enemyIndex] = EPT_Visible;
		enemiesProbCount[enemyIndex].insert( PointToIndex( enemy.X, enemy.Y, enemy.Stance ) );

		for( int j = 0; j < enemiesProbCount.size(); j++ ) {		//Clear other 
			if( j == enemyIndex ) {
				continue;
			}
			enemiesProbCount[j].erase( PointToIndex( enemy.X, enemy.Y, 0 ) );
			enemiesProbCount[j].erase( PointToIndex( enemy.X, enemy.Y, 1 ) );
			enemiesProbCount[j].erase( PointToIndex( enemy.X, enemy.Y, 2 ) );
		}

	}

}

void CEnemyProb::UpdateVisibleCells()
{
	for( int i = 0; i < countOfEnemies; i++ ) {
		CTrooperVariant enemy;
		enemy.Type = getTypeByIndex( i );
		if( enemyTypes[i] == EPT_Visible ) {
			continue;
		}
		for( int x = 0; x < GetCurrentState().GetWidth(); x++ ) {
			for( int y = 0; y < GetCurrentState().GetHeight(); y++ ) {
				for( int stance = 0; stance <=2; stance++ ) {
					const int index = PointToIndex( x, y, stance );
					enemy.X = x;
					enemy.Y = y;
					enemy.Stance = stance;
					if( !GetCurrentState().IsCellFree( x, y ) || IsEnemyVisible( enemy ) ) {
						enemiesProbCount[i].erase( index );
					}
				}
			}
		}
	}
}

long long CEnemyProb::selectEnemyIdByDamage( TTrooperType myTrooperType ) const
{
	vector<int> scoreDelta( teamSize, 0 );
	const long long myPlayerId = GetCurrentState().GetCurrentTrooper().PlayerId;
	const vector<Player>& players = GetCurrentState().world->getPlayers();
	for( int i = 0; i < players.size(); i++ ) {
		if( players[i].getId() != myPlayerId ) {
			const int teamId = GetTeamIndex( players[i].getId() );
			const int score = players[i].getScore();
			scoreDelta[teamId] = score;
		}
	}
	for( int i = 0; i < teamSize; i++ ) {
		scoreDelta[i] -= enemiesPoints[i];
	}
	
	int totalPoints = 0;
	int curPoints = 0;
	for( int i = 0; i < TT_Count; i++ ) {
		int damage = GetCurrentState().GetTrooperDamage( static_cast<TTrooperType>( i ) );
		bool isKilled = damage > 0 && !GetCurrentState().IsTrooperAlive( static_cast<TTrooperType>( i ) );
		const int points = damage + ( isKilled ? 50 : 0 );
		totalPoints += points;
		if( i == myTrooperType ) {
			curPoints += points;
		}
	}
	
	if( DumpLog ) {
		cout << "ENEMIES SCORE DELTA:" << endl;
		const vector<Player>& players = GetCurrentState().world->getPlayers();
		for( int i = 0; i < players.size(); i++ ) {
			if( players[i].getId() != myPlayerId ) {
				const int teamId = GetTeamIndex( players[i].getId() );
				cout << players[i].getName() << "( " << players[i].getId() << ") " << scoreDelta[teamId] << endl;
			}
		}
		cout << "TOTAL POINTS: " << totalPoints << ", curPoints: " << curPoints << endl;
	}

	for( int i = 0; i < teamSize; i++ ) {
		if( scoreDelta[i] == totalPoints ) {
			return GetEnemyIdByIndex( i );
		}
	}
	for( int i = 0; i < teamSize; i++ ) {
		if( scoreDelta[i] == curPoints ) {
			return GetEnemyIdByIndex( i );
		}
	}
	for( int i = 0; i < teamSize; i++ ) {
		if( scoreDelta[i] > totalPoints ) {
			return GetEnemyIdByIndex( i );
		}
	}
	long long bestId = GetCurrentState().GetLastEnemyId();
	long long maxDamage = 0;
	for( int i = 0; i < teamSize; i++ ) {
		if( scoreDelta[i] > maxDamage ) {
			maxDamage = scoreDelta[i];
			bestId = GetEnemyIdByIndex( i );
		}
	}
	return bestId;
}

bool CEnemyProb::buildVariantsEnemyDamage( TTrooperType damagedType, long long enemyId, bool onlyOneTrooper, bool isOrderChanged )
{
	variantsEnemyType.clear();
	variantsMinShoot.clear();
	variantsGrenade.clear();
	variantsStanceMask.clear();
	
	bool isEnemyMovesFirst = GetCurrentState().GetPlayerOrder( enemyId ) == -1;
	if( isOrderChanged ) {
		isEnemyMovesFirst = !isEnemyMovesFirst;
	}
	const int firstSubTurn = ( GetCurrentState().PredSubTurnNumber + ( isEnemyMovesFirst ? 1 : 0 ) ) % TT_Count;
	const int lastSubTurn = ( GetCurrentState().SubTurnNumber + ( isEnemyMovesFirst ? 1 : 0 ) ) % TT_Count;
	
	const int damage = GetCurrentState().GetTrooperDamage( damagedType );
	const bool isKilled = !GetCurrentState().IsTrooperAlive( damagedType );
	if( DumpLog ) {
		cout << "Start find enemies from damage for " << CTrooperVariant::GetTrooperTypeString( damagedType ) << endl;
	}
	CTrooperVariant enemy;
	for( int i = 0; i < TT_Count; i++ ) {
		const int subTurn = GetCurrentState().GetSubTurnForTrooper( static_cast<TTrooperType>( i ) );
		if( ( firstSubTurn < lastSubTurn && subTurn >= firstSubTurn && subTurn < lastSubTurn ) ||
			( firstSubTurn >= lastSubTurn && ( subTurn >= firstSubTurn || subTurn < lastSubTurn ) ) )
		{
			TTrooperType enemyType = static_cast<TTrooperType>( i );
			enemy.Type = enemyType;
			if( isKilled ) {
				if( damage <= 80 || !onlyOneTrooper ) {
					variantsEnemyType.push_back( enemyType );
					variantsMinShoot.push_back( 0 );
					variantsGrenade.push_back( true );
					variantsStanceMask.push_back( 0 );
				}
				for( int stance = 0; stance <= 2; stance++ ) {
					enemy.Stance = stance;
					const int shootDamage = enemy.GetShootDamage();
					const int shootCount = damage / shootDamage + ( damage % shootDamage > 0 ? 1 : 0 );
					int movesCount = CMove::ActionPointsToMove( enemy.Type, MT_Shoot, stance );
					if( movesCount * shootCount <= 12 || !onlyOneTrooper ) {
						variantsEnemyType.push_back( enemyType );
						variantsMinShoot.push_back( onlyOneTrooper ? shootCount : 1 );
						variantsGrenade.push_back( false );
						variantsStanceMask.push_back( stance );
					}
				}
			} else {
				if( ( damage == 80 || damage == 60 ) || ( !onlyOneTrooper && ( damage <= 80 || damage <= 60 ) ) ) {
					variantsEnemyType.push_back( enemyType );
					variantsMinShoot.push_back( 0 );
					variantsGrenade.push_back( true );
					variantsStanceMask.push_back( 0 );
				}
				for( int stance = 0; stance <= 2; stance++ ) {
					enemy.Stance = stance;
					const int shootDamage = enemy.GetShootDamage();
					const int shootCount = damage / shootDamage + ( damage % shootDamage > 0 ? 1 : 0 );
					int movesCount = CMove::ActionPointsToMove( enemy.Type, MT_Shoot, stance );
					if( ( !onlyOneTrooper && shootDamage <= damage ) || ( onlyOneTrooper && movesCount * shootCount <= 12 && damage % shootDamage == 0 ) ) {
						variantsEnemyType.push_back( enemyType );
						variantsMinShoot.push_back( onlyOneTrooper ? shootCount : 1 );
						variantsGrenade.push_back( false );
						variantsStanceMask.push_back( stance );
						if( DumpLog ) {
							cout << "Add trooper: ";
							enemy.Log();
							cout << ", shootDamage=" << shootDamage << ", shootCount=" << shootCount << ", stance=" << stance << endl;
						}
					}
				}
			}
		}
	}
	return variantsEnemyType.size() > 0;
}

void CEnemyProb::UpdateByDamage()
{
	const int width = GetCurrentState().GetWidth();
	const int height = GetCurrentState().GetHeight();
	CTrooperVariant enemy;
	for( int t = 0; t < TT_Count; t++ ) {
		//Select enemy variants:
		const TTrooperType damageType = static_cast<TTrooperType>( t );
		if( GetCurrentState().GetTrooperDamage( damageType ) == 0  ) {
			continue;
		}
		long long enemyId = selectEnemyIdByDamage( damageType );
		if( DumpLog ) {
			cout << "Select enemy for damage: " << enemyId << endl;
		}
		if( !buildVariantsEnemyDamage( damageType, enemyId, true, false ) ) {
			if( !buildVariantsEnemyDamage( damageType, enemyId, false, false ) ) {
				if( buildVariantsEnemyDamage( damageType, enemyId, true, true ) ) {
					GetCurrentState().changePlayerPos( enemyId );
				}
			}
		}

		CTrooperVariant damagedTrooper;
		damagedTrooper.Type = static_cast<TTrooperType>( t );
		IndexToPoint( GetCurrentState().GetTrooperLastTurnPosition( damagedTrooper.Type ),
				damagedTrooper.X, damagedTrooper.Y, damagedTrooper.Stance );

		vector<bool> isProcessed( TT_Count, false );
		for( int i = 0; i < variantsEnemyType.size(); i++ ) {
			const int enemyIndex = GetEnemyIndex( enemyId, variantsEnemyType[i] );

			if( GetCurrentState().EnemyInfo().IsVisible( enemyIndex ) ||
					enemyTypes[enemyIndex] == EPT_Visible ||
					!GetCurrentState().EnemyInfo().IsAlive( enemyIndex ) )
			{
				continue;
			}
			enemy.Type = static_cast<TTrooperType>( variantsEnemyType[i] ); 
			if( !isProcessed[enemy.Type] ) {
				clearProb( enemyIndex );
				enemyTypes[enemyIndex] = EPT_FromDamage;
				isProcessed[enemy.Type] = true;
			}

			if( variantsGrenade[i] ) {
				for( int x = max( 0, damagedTrooper.X - 7 ); x <= min( width - 1, damagedTrooper.X + 7 ); x++ ) {
					for( int y = max( 0, damagedTrooper.Y - 7); y <= min( height - 1, damagedTrooper.Y + 7 ); y++ ) {
						for( int stance = 0; stance <= 2; stance++ ) {
							enemy.X = x;
							enemy.Y = y;
							enemy.Stance = stance;
							if( GetCurrentState().IsCellFree( x, y ) && GetCurrentState().GetDistanceEuk( x, y, damagedTrooper.X, damagedTrooper.Y ) < 7 + 1e-5 &&
									!IsEnemyVisible( enemy ) )
							{
								enemiesProbCount[enemyIndex].insert( PointToIndex( x, y, stance ) );
							}
						}
					}
				}
			} else {
				CMinMovesToShoot minMovesToShoot( damagedTrooper );
				minMovesToShoot.BuildFromShooterToTrooper( 2, enemy, false );
				const int pointsPerShoot = variantsMinShoot[i] * CMove::ActionPointsToMove( enemy.Type, MT_Shoot, 0 );
				const int maxMoves = ( 12 - pointsPerShoot ) / 2;
				for( map<int, int>::const_iterator it = minMovesToShoot.GetMinMoves().begin();
						it != minMovesToShoot.GetMinMoves().end(); it++ )
				{
					IndexToPoint( it->first, enemy.X, enemy.Y, enemy.Stance );
					int addMovesToStance = 0;
					if( it->second == 0 ) {
						addMovesToStance += 2 * abs ( enemy.Stance - variantsStanceMask[i] );
					} else {
						addMovesToStance += 2 * ( 2 - variantsStanceMask[i] );
					}
					if( it->second > maxMoves - addMovesToStance ) {
						continue;
					}
					if( GetCurrentState().IsCellFree( enemy.X, enemy.Y ) && !IsEnemyVisible( enemy ) ) {
						enemiesProbCount[enemyIndex].insert( it->first );
					}
				}
			}

			if( DumpLog ) {
				if( enemiesProbCount[enemyIndex].size() > 0 ) {
					cout << "ADD ENEMY FROM DAMAGE: " << enemiesProbCount[enemyIndex].size() << endl;
				}
			}
		}
	}
}

void CEnemyProb::clearProb( int enemyIndex )
{
	enemiesProbCount[enemyIndex].clear();
	lastUpdatedTurn[enemyIndex] = GetCurrentState().TurnNumber;

}

void CEnemyProb::clearProbsForUnknownEnemies( int teamId )
{
	for( int i = 0; i < enemiesProbCount.size(); i++ ) {
		if( i / TT_Count == teamId && enemyTypes[i] == EPT_Unknown ) {
			clearProb( i );
		}
	}
}

void CEnemyProb::clearOldProbs()
{
	const int turnNumber = GetCurrentState().TurnNumber;
	for( int i = 0; i < enemiesProbCount.size(); i++ ) {
		if( enemyTypes[i] == EPT_Visible || 
				!GetCurrentState().EnemyInfo().IsAlive( i ) ||
				turnNumber > lastUpdatedTurn[i] + 3 )
		{
			clearProb( i );
			continue;
		}
		
		const long long enemyId = GetEnemyIdByIndex( i );
		const TTrooperType trooperType = static_cast<TTrooperType>( i % TT_Count );
		if( enemyTypes[i] == EPT_Unknown && ( lastUpdatedTurn[i] < turnNumber - 1 ||
			( lastUpdatedTurn[i] == turnNumber - 1 && GetCurrentState().GetSubTurnForEnemy( enemyId, trooperType ) < GetCurrentState().SubTurnNumber ) ) )
		{
			clearProb( i );
			continue;
		}
	}

	for( int i = 0; i < enemiesCenters.size(); i++ ) {
		if( lastTurnNumberForEnemiesCenter[i] < GetCurrentState().TurnNumber - 4 ) {
			enemiesCenters[i] = make_pair( -1, -1 );
		}
	}
}

void CEnemyProb::SetEnemiesCenter( int teamId, int x, int y )
{
	enemiesCenters[teamId].first = x;
	enemiesCenters[teamId].second = y;
}

void CEnemyProb::recalculateEnemiesCenter()
{
	for( int teamId = 0; teamId < teamSize; teamId++ ) {
		double newX = 0;
		double newY = 0;
		int count = 0;
		for( int i = 0; i < GetCurrentState().GetEnemyTroopers().size(); i++ ) {
			const CTrooperVariant& enemy = GetCurrentState().GetEnemyTroopers()[i];
			if(	teamId != GetTeamIndex( enemy.PlayerId ) ) {
				continue;
			}
			newX += enemy.X;
			newY += enemy.Y;
			count++;	
		}
		if( count > 0 ) {
			enemiesCenters[teamId].first = newX / count + 0.5;
			enemiesCenters[teamId].second = newY / count + 0.5;
			lastTurnNumberForEnemiesCenter[teamId] = GetCurrentState().TurnNumber;
			clearProbsForUnknownEnemies( teamId );
		}
	}
}

void CEnemyProb::addInvisibleEnemiesNearCenter()
{
	const int radius = 3;
	const int width = GetCurrentState().GetWidth();
	const int height = GetCurrentState().GetHeight();
	for( int teamId = 0; teamId < teamSize; teamId++ ) {
		int enemiesCenterX = enemiesCenters[teamId].first;
		int enemiesCenterY = enemiesCenters[teamId].second;
		if( enemiesCenterX == -1 || enemiesCenterY == -1 ) {
			continue;
		}
		for( int i = 0; i < enemiesProbCount.size(); i++ ) {
			if( teamId != i / TT_Count || enemiesProbCount[i].size() > 0 || !GetCurrentState().EnemyInfo().IsAlive( i ) ) {
				continue;
			}
			CTrooperVariant enemy;
			enemy.Type = getTypeByIndex( i );
			enemyTypes[i] = EPT_Unknown;
			lastUpdatedTurn[i] = GetCurrentState().TurnNumber;
			if( GetCurrentState().GetSubTurnForEnemy( GetEnemyIdByIndex( i ), enemy.Type ) >= GetCurrentState().SubTurnNumber ) {
				lastUpdatedTurn[i]--;
			}
			const set<int> &visibleSet = enemy.Type == TT_Sniper ? GetCurrentState().GetThisSubturnVisibleSetSniper() :  GetCurrentState().GetThisSubturnVisibleSet();
			for( int x = max( 0, enemiesCenterX - radius ); x <= min( width - 1, enemiesCenterX + radius ); x++ ) {
				for( int y = max( 0, enemiesCenterY - radius ); y <= min( height - 1, enemiesCenterY + radius ); y++ ) {
					if( !GetCurrentState().IsCellFree( x, y ) ) {
						continue;
					}
					for( int stance = 0; stance <= 2; stance++ ) {
						enemy.X = x;
						enemy.Y = y;
						enemy.Stance = stance;
						if( visibleSet.find( PointToIndex( x, y, stance ) ) == visibleSet.end() ) {
							enemiesProbCount[i].insert( PointToIndex( x, y, stance ) );	
						} else {
							break;
						}
					}
				}
			}
		}
	}
}

void CEnemyProb::dumpLog()
{
	if( !DumpLog || !GetCurrentState().IsNextTrooperMove() ) {
		return;
	}
	cout << "ENEMY PROB:" << endl;
	const long long myPlayerId = GetCurrentState().GetCurrentTrooper().PlayerId;
	const vector<Player>& players = GetCurrentState().world->getPlayers();
	for( int playerId = 0; playerId < players.size(); playerId++ ) {
		if( players[playerId].getId() == myPlayerId ) {
			continue;
		}
		const int centerX = enemiesCenters[GetTeamIndex( players[playerId].getId() )].first;
		const int centerY = enemiesCenters[GetTeamIndex( players[playerId].getId() )].second;
		cout << "troopers by " << players[playerId].getName() << "( center: " << centerX << ", " << centerY << "):" << endl;
		for( int i = 0; i < TT_Count; i++ ) {
			const int enemyIndex = GetEnemyIndex( players[playerId].getId(), static_cast<TTrooperType>( i ) );
			const int count = enemiesProbCount[enemyIndex].size();
			if( count > 0 ) {
				string probTypeString = "";
				if( enemyTypes[enemyIndex] == EPT_Visible ) {
					probTypeString = "Visible";
				} else if( enemyTypes[enemyIndex] == EPT_FromDamage ) {
					probTypeString = "Damage";
				} else {
					probTypeString = "Unknown";
				}
				cout << "Trooper: " << CTrooperVariant::GetTrooperTypeString( static_cast<TTrooperType>( i ) ) << " Type: " << probTypeString << " Count: " << count << endl; 
			}
		}
	}
}
