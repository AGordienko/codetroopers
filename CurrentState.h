#pragma once
#ifndef CURRENTSTATE_H
#define CURRENTSTATE_H

#include "Strategy.h"
#include "TrooperVariant.h"
#include "Tactic.h"
#include "EnemyProb.h"

enum TGameType {
	GT_Three,
	GT_Four,
	GT_Five
};

enum TCurrentState {
	CS_Still,
	CS_Enemy,
	CS_Fight
};

enum TCellInfo {
	CI_Free = 0,
	CI_Medict = 1,
	CI_Grenage = 1 << 1,
	CI_Ration = 1 << 2,
	
	CI_MyTrooper = 1 << 3,
	CI_EnemyTrooper = 1 << 4,
	
	CI_CoverLow = 1 << 5,
	CI_CoverMedium = 1 << 6,
	CI_CoverHigh = 1 << 7
};

class CEnemyInfo {
public:
	void Initialize();

	void Update();
	bool IsVisible( int playerId ) const { return isVisible[playerId]; }
	bool IsAlive( int playerId ) const { return isAlive[playerId]; }
	void SetKilled( int playerId ) { isAlive[playerId] = false; }

private:
	vector<bool> isVisible;
	vector<bool> isAlive;
};

inline bool HasCellBonus( TCellInfo cellInfo ) { return (cellInfo & ( CI_Medict | CI_Grenage | CI_Ration )) != 0; }

const int NonReachable = 100000000;

class CCurrentState {
public:
	CCurrentState();
	
	const World* world;
	const Game* game;

	TTrooperType PredTrooperType;
	int TurnNumber;
	int SubTurnNumber;
	int PredSubTurnNumber;
		
	int LastTurnShootDamage;
	TTrooperType LastTurnShootToEnemy;
	long long LastTurnShootToEnemyId;
	int LastTurnScore;
	
	int RandomShoot;
		
	ITactic* CurrentTactic;
	
	TCurrentState CurrentState;
	
	vector<CTrooperVariant> MyTroopers;

/////////////

	void Update( const World& _world, const Game& _game, int _currentTrooperIndex );

	const vector<CTrooperVariant>& GetMyTroopers() const { return myTroopers; }	
	const vector<CTrooperVariant>& GetEnemyTroopers() const { return enemyTroopers; }	
	const CTrooperVariant& GetCurrentTrooper() const { return currentTrooper; }
	const CTrooperVariant& GetTrooperAtStartSubTurn() const { return trooperAtStartSubTurn; }

	TCellInfo GetCellInfo( int x, int y ) const { return worldMap[pointToIndex( x, y ) ]; }
	int GetWidth() const { return width; }
	int GetHeight() const { return height; }
	bool IsCellFree( int x, int y ) const { return worldMap[pointToIndex( x, y )] < CI_MyTrooper; }
	bool IsCellWithoutCover( int x, int y ) const { return worldMap[pointToIndex(x, y ) ] < CI_CoverLow; }
	bool IsEnemyVisible( const CTrooperVariant& me, const CTrooperVariant& enemy ) const;	
	bool CanShootToEnemy( const CTrooperVariant& me, const CTrooperVariant& enemy ) const;
	int GetDistance( int x0, int y0, int x1, int y1 ) {
		return distances[pointToIndex( x0, y0 )][pointToIndex( x1, y1 )];
	}
	double GetDistanceEuk( int x0, int y0, int x1, int y1 ) {
		return distancesEuk[pointToIndex( x0, y0 )][pointToIndex( x1, y1 )];
	}
	
	bool IsTrooperAlive( TTrooperType trooperType ) const { return isAlive[trooperType]; }
	
	void SaveEnemyTroopers( const CMove& move );
	bool IsNextTrooperMove() const { return isNextTrooperMove; }	
	
	TGameType GameType() const { return gameType; }
	const CEnemyInfo& EnemyInfo() const { return enemyInfo; }
	CEnemyInfo& EnemyInfo() { return enemyInfo; }
	const CEnemyProb& EnemiesProb() const { return enemiesProb; }
	
	int GetTrooperLastTurnPosition( TTrooperType trooperType ) const { return myTroopersPosition[trooperType]; }
	int GetPlayerOrder( long long id ) { return playersOrder[id]; }
	int GetTrooperDamage( TTrooperType trooperType ) const { return damageToMyTroopers[trooperType]; }
	int GetSubTurnForTrooper( TTrooperType trooperType ) const { return troopersMoveOrder[trooperType]; }	
	
	const set<int>& GetThisSubturnVisibleSet() const { return thisSubturnVisibleSet; }
	const set<int>& GetThisSubturnVisibleSetSniper() const { return thisSubturnVisibleSetSniper; }
	
	
	bool IsEnemyMoveFirst( const CTrooperVariant& me, const CTrooperVariant& enemy );
	int GetSubTurnForEnemy( long long enemyId, TTrooperType trooperType ) { return troopersMoveOrder[trooperType] + playersOrder[enemyId]; }
	int EnemyMoveOrder( const CTrooperVariant& enemy );
	
	const set<int>& GetVisibleSetForTrooper( const CTrooperVariant& trooper, bool isEnemySniper );
	long long GetLastEnemyId() const { return lastEnemyId; }
	
	void changePlayerPos( long long playerId);

private:
	//Troopers info:
	int currentTrooperIndex;	
	vector< CTrooperVariant> myTroopers;
	vector< CTrooperVariant> enemyTroopers;
	CTrooperVariant currentTrooper;
	CTrooperVariant trooperAtStartSubTurn;
	vector<bool> isAlive;
	vector<int> troopersMoveOrder;
	bool isNewTurn;
	bool isNextTrooperMove;

	TGameType gameType;
	CEnemyInfo enemyInfo;
	CEnemyProb enemiesProb;
	vector<int> damageToMyTroopers;
	vector<int> myTroopersPosition;
				
	//Players order and saved troopers:
	map<long long, int> playersOrder;
	int firstInvisibleTrooperIndex;	
	vector<CTrooperVariant> savedEnemyTroopers;
	
	//Precalculated distances:
	int width;
	int height;
	vector<TCellInfo> worldMap;
	vector< vector<int> > distances;
	vector< vector<double > > distancesEuk;

	set<int> thisSubturnVisibleSet;
	set<int> thisSubturnVisibleSetSniper;
	
	map<int, set<int> > savedVisibleSetForTrooper;
	
	long long lastEnemyId;

	void updateTroopers();
	void updateBonuses();

	int pointToIndex( int x, int y ) const { return y * width + x; }
	void indexToPoint( int index, int &x, int &y ) const { x = index % width; y = index / width; }
	bool hasCoverAtCell( int index ) const { return worldMap[index] >= CI_CoverLow; }

	static bool isInitialized;
	void initialize();
	void countDistances();
	void countDistancesEuk();

	void processSavedEnemyTroopers();


	void updateVisibleSet();
	
	void addVisiblesCell( set<int>& result, const CTrooperVariant& trooper, bool isEnemySniper ) const;

	void dump();
};

CCurrentState& GetCurrentState();

bool IsEnemyVisible( const CTrooperVariant& enemy, const vector<CTrooperVariant>& friends, const CTrooperVariant& me );
inline bool IsEnemyVisible( const CTrooperVariant& enemy) { return IsEnemyVisible( enemy, GetCurrentState().GetMyTroopers(), GetCurrentState().GetCurrentTrooper() ); }

int getScore( long long playerId );
int GetMyScore();
int GetEnemyScore();
long long getEnemyId();

int GetEnemyIndex( long long playerId, TTrooperType type );
long long GetEnemyIdByIndex( int enemyIndex );
int GetTeamIndex( long long playerId );
#endif //CURRENTSTATE_H
