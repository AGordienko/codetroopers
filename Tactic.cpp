#include "Tactic.h"
#include "CurrentState.h"
#include "MinMovesToShoot.h"

int CExplorationTactic::countOfTurnsLeftBeforePlane;
int CExplorationTactic::predTurnNumber;

CExplorationTactic::~CExplorationTactic()
{
	for( int i = 0; i < minMovesBuffer.size(); i++ ) {
		delete minMovesBuffer[i];
	}
}

bool CExplorationTactic::IsMoveUseless( const CMove& move ) const
{
	switch( move.MoveType ) {
		case MT_Shoot :
		case MT_Skip :
		case MT_UseGrenade :
		case MT_UseRation :
			return true;
			break;
/*		case MT_Stand :
			if( move.Y < 0 || GetCurrentState().GetCurrentTrooper().Stance == 2 ) {
				return true;
			}
			break;*/
		case MT_UseMedikt :
			if( isMedicAlive ) {
				return true;
			}
			break;
		case MT_GetRadarInfo :
			return GetCurrentState().GetCurrentTrooper().Type != TT_Commander;
			break;
	}
	return false;
}

double CExplorationTactic::GetPositionQuality( const CTrooperVariant& trooperVariant,
		bool dumpToLog, double bestQuality ) const
{
	const int MediktBonus = 6;
	const int GrenadeBonus = 6;
	const int RationBonus = 3;
	const double WoundsPenalty = 0.4; 
	const int MedicDistancePenalty = 2;
	const int CommandirTooFarPenalty = -1;
	const int NonMovingPenalty = -4;

	double result = 0;
	double distanceBonus = 0;
	int leaderingPenalty = 0;
	int radarRequestBonus = 0;
	const vector<CTrooperVariant>& myTroopers = GetCurrentState().GetMyTroopers();
	
	if( trooperVariant.MovesLog.size() > 0 && trooperVariant.MovesLog[0].MoveType == MT_GetRadarInfo ) {
		if( countOfTurnsLeftBeforePlane <= 0 ) {
			radarRequestBonus = 10;
		} else {
			return -1000;
		}
	}
	const int distance = GetCurrentState().GetDistance( targetX, targetY, trooperVariant.X, trooperVariant.Y );
	const int distanceToCommandir = GetCurrentState().GetDistance( trooperVariant.X, trooperVariant.Y, commander->X, commander->Y );
	int nonMovingPenalty = 0;	
	if( isCommander ) {
		distanceBonus = -distance * 0.5;
		int maxDistanceToTrooper = 0;
		for( int i = 0; i < myTroopers.size(); i++ ) {
			maxDistanceToTrooper = max( maxDistanceToTrooper, GetCurrentState().GetDistance( trooperVariant.X, trooperVariant.Y, myTroopers[i].X, myTroopers[i].Y ) );
		}
		distanceBonus += max( 0, maxDistanceToTrooper - 5 ) * CommandirTooFarPenalty;
		if( trooperVariant.X == GetCurrentState().GetTrooperAtStartSubTurn().X && trooperVariant.Y == GetCurrentState().GetTrooperAtStartSubTurn().Y ) {
			nonMovingPenalty += NonMovingPenalty;
		}
		
		leaderingPenalty += max( 0, startDistanceForLeader - distance - 3 ) * CommandirTooFarPenalty;

	} else {
		distanceBonus = -( distanceToCommandir - 1 );
		leaderingPenalty = min( 0, distance - distanceToTargetForCommander );

		//To prevent blocking:
		if( leaderingPenalty < 0 ) {
			if( trooperVariant.X == GetCurrentState().GetTrooperAtStartSubTurn().X && trooperVariant.Y == GetCurrentState().GetTrooperAtStartSubTurn().Y ) {
				nonMovingPenalty += NonMovingPenalty;
			}
			if( distanceToCommandir < 3 ) {
				leaderingPenalty -= 3;
			}
		}
	}

	int itemsBonus = 0;
	if( trooperVariant.HasMedikt ) {
		itemsBonus += MediktBonus;
	}
	if( trooperVariant.HasGrenade ) {
		itemsBonus += GrenadeBonus;
	}
	if( trooperVariant.HasRation ) {
		itemsBonus += RationBonus;
	}

	int countOfWounds = max( 0, 100 - trooperVariant.Health );
	for( int i = 0; i < trooperVariant.FriendsHealth.size(); i++ ) {
		countOfWounds += max( 0, 100 - trooperVariant.FriendsHealth[i] );
	}
	double healthBonus = -countOfWounds * WoundsPenalty;
	if( isMedicAlive  && curTrooperType != TT_Medic && trooperVariant.Health < 100 ) {
		const int distanceToMedic = GetCurrentState().GetDistance( trooperVariant.X, trooperVariant.Y, medic->X, medic->Y ) - 1;
		healthBonus += -distanceToMedic * MedicDistancePenalty;
	}
	
	int stancePenalty = 2 * ( 2 - trooperVariant.Stance );
	
	int bestDist = distance;
	CTrooperVariant trooperForScoutingBonus = GetCurrentState().GetCurrentTrooper();
	for( int i = 0; i <= trooperVariant.MovesLog.size(); i++ ) {
		const int testDistance = GetCurrentState().GetDistance( targetX, targetY,
			trooperForScoutingBonus.X, trooperForScoutingBonus.Y );
		bestDist = min( bestDist, testDistance );
		if( i < trooperVariant.MovesLog.size() ) {
			trooperVariant.MovesLog[i].Apply( trooperForScoutingBonus ); 
		}
	}
	trooperVariant.ScoutingBonus = bestDist;
	double explorationPenalty = bestDist / 10000.;
//	double explorationPenalty = 0;
	
	vector< const set<int>*> visibleSet;
	vector< const set<int>*> visibleSetSniper;
	buildVisibleSet( visibleSet, visibleSetSniper, trooperVariant, 6 );
	int countOfGoodCells = 0;
/*
	for( set<int>::const_iterator it = visibleSet.begin(); it != visibleSet.end(); it++ ) {
		int x, y, stance;
		IndexToPoint( *it, x, y, stance );
		const int distanceToTarget = GetCurrentState().GetDistance( targetX, targetY, x, y );
		if( distanceToTarget <= distance ) {
			countOfGoodCells++;
		}
	}
	explorationPenalty = countOfGoodCells / 10000.;
*/
	if( GetCurrentState().EnemiesProb().HasEnemies() ) {
		leaderingPenalty = 0;
	//	nonMovingPenalty = 0;
	//	stancePenalty = 0;
	}

	result = distanceBonus + leaderingPenalty + itemsBonus + healthBonus + nonMovingPenalty - explorationPenalty + radarRequestBonus - stancePenalty;
	if( result < bestQuality - 20 ) {
		return TacticWorstQuality;
	}
	double explorationBonus = getExplorationBonus( trooperVariant );
	result += explorationBonus;
	if( result < bestQuality ) {
		return TacticWorstQuality;
	}
	double invisibleEnemiesPenalty = getPenaltyForInvisibleEnemies( trooperVariant, visibleSet, visibleSetSniper );
	result -= invisibleEnemiesPenalty;


	if( DumpLog ) {
		if( dumpToLog ) {
			for( int i = 0; i < trooperVariant.MovesLog.size(); i++ ) {
				trooperVariant.MovesLog[i].Log();
				cout << ' ';
			}
			cout << "quality=" << result << "(distance=" << distanceBonus <<
			 ", leaderingPenalty=" << leaderingPenalty << ", itemsBonus=" << itemsBonus <<
			 ", healthBonus=" << healthBonus << ", nonMovingPenalty=" << nonMovingPenalty <<
			 ", explorationPenalty=" << explorationPenalty << ", radarRequestBonus=" << radarRequestBonus <<
			 ", invisibleEnemiesPenalty=" << invisibleEnemiesPenalty << ", explorationBonus=" << explorationBonus << endl;
		}
	}
	return result;
}

void CExplorationTactic::Initialize()
{
	GetCheckpoints().GetCurTarget( targetX, targetY );
	const CTrooperVariant& curTrooper = GetCurrentState().GetCurrentTrooper();
	bool isVisible = GetCurrentState().world->isVisible( 3, curTrooper.X, curTrooper.Y, curTrooper.ConvertStance(), targetX, targetY, PRONE ); 
	if( isVisible ) {
		GetCheckpoints().SetNextTarget();
		GetCheckpoints().GetCurTarget( targetX, targetY );
	}

	// Process Radar info:
	if( GetCurrentState().TurnNumber == 0 ) {
		countOfTurnsLeftBeforePlane = 5;
		predTurnNumber = 0;
	}
	
	if( GetCurrentState().GetCurrentTrooper().Type == TT_Commander && GetCurrentState().TurnNumber > predTurnNumber ) {
		if( predTurnNumber == GetCurrentState().TurnNumber - 1 ) {
			countOfTurnsLeftBeforePlane--;
		} else {
			countOfTurnsLeftBeforePlane = 5;
		}

		if( countOfTurnsLeftBeforePlane < 0 ) {
			const vector<Player>& players = GetCurrentState().world->getPlayers();
			const long long myPlayerId = curTrooper.PlayerId;
			int bestX = -1;
			int bestY = -1;
			int bestDist = NonReachable;
			for( int i = 0; i < players.size(); i++ ) {
				if( players[i].getId() == myPlayerId ) {
					continue;
				}
				const int approxX = players[i].getApproximateX();
				const int approxY = players[i].getApproximateY();
				if( approxX == -1 || approxY == -1 ) {
					continue;
				}
				int emptyCellX = -1;
				int emptyCellY = -1;
				if( GetCurrentState().IsCellFree( approxX, approxY ) ) { 
					emptyCellX = approxX;
					emptyCellY = approxY;
				} else {
					int minDistToApprox = NonReachable;
					//Bad code:
					for( int x = 0; x < GetCurrentState().GetWidth(); x++ ) {
						for( int y = 0; y < GetCurrentState().GetHeight(); y++ ) {
							const int dist = abs( x - approxX ) + abs( y - approxY );
							if( dist < minDistToApprox && GetCurrentState().IsCellFree( x, y ) ) {
								minDistToApprox = dist;
								emptyCellX = x;
								emptyCellY = y;
							}
						}
					}
				}
				if( emptyCellX == -1 || emptyCellY == -1 ) {
					continue;
				}
				const int dist = GetCurrentState().GetDistance( curTrooper.X, curTrooper.Y, emptyCellX, emptyCellY );
				if( dist < bestDist  && dist > 2 ) {
					bestDist = dist;
					bestX = emptyCellX;
					bestY = emptyCellY;
				}
			}
			if( bestX != -1 && bestY != -1 ) {
				GetCheckpoints().SetCustomTarget( bestX, bestY );
				GetCheckpoints().GetCurTarget( targetX, targetY );
				if( DumpLog ) {
					cout << "USE RADAR: " << targetX << ", " << targetY << endl;
				}
				countOfTurnsLeftBeforePlane = 5;
			}
		}
		predTurnNumber = GetCurrentState().TurnNumber;
	}

	isMedicAlive = GetCurrentState().IsTrooperAlive( TT_Medic );
	TTrooperType commanderTrooperType;
	if( GetCurrentState().IsTrooperAlive( TT_Scout ) ) {
		commanderTrooperType = TT_Scout;
	} else if( GetCurrentState().IsTrooperAlive( TT_Commander ) ) {
		commanderTrooperType = TT_Commander;
	} else if( GetCurrentState().IsTrooperAlive( TT_Soldier ) ) {
		commanderTrooperType = TT_Soldier;
	} else if( GetCurrentState().IsTrooperAlive( TT_Medic ) ) {
		commanderTrooperType = TT_Medic;
	} else if( GetCurrentState().IsTrooperAlive( TT_Sniper ) ) {
		commanderTrooperType = TT_Sniper;
	} else {
		assert( false );
	}
	isCommander = curTrooper.Type == commanderTrooperType;
	curTrooperType = curTrooper.Type;
	const vector<CTrooperVariant>& myTroopers = GetCurrentState().GetMyTroopers();

	commander = 0;
	medic = 0;
	const CTrooperVariant& trooperAtStart = GetCurrentState().GetTrooperAtStartSubTurn();
	if( isCommander ) {
		commander = &curTrooper;
		startDistanceForLeader = GetCurrentState().GetDistance( targetX, targetY, trooperAtStart.X, trooperAtStart.Y );
	}
	startHashForTrooper = trooperAtStart.GetHash(); 
	if( curTrooperType == TT_Medic ) {
		medic = &curTrooper;
	}

	for( int i = 0; i < myTroopers.size(); i++ ) {
		const CTrooperVariant &trooper = myTroopers[i];
		if( trooper.Type == commanderTrooperType ) {
			commander = &trooper;
		}
		if( trooper.Type == TT_Medic ) {
			medic = &trooper;
		}
	}
	distanceToTargetForCommander = GetCurrentState().GetDistance(
			targetX, targetY, commander->X, commander->Y );
	

	if( DumpLog ) {	
		cout << "Target: (" << targetX << ", " << targetY << ")" << endl;
	}
	
	minMovesToShootForInvisibleEnemies.resize( TT_Count );
}

void CExplorationTactic::buildVisibleSet( vector<const set<int>*>& result, vector<const set<int>*>& resultSniper, const CTrooperVariant& currentTrooper, int minMovesLeft ) const
{
	CTrooperVariant testVar = GetCurrentState().GetCurrentTrooper();
	result.clear();
	resultSniper.clear();
	result.push_back( &GetCurrentState().GetThisSubturnVisibleSet() );
	resultSniper.push_back( &GetCurrentState().GetThisSubturnVisibleSetSniper() );
	bool isPosChange = false;
	for( int i = 0; i <= currentTrooper.MovesLog.size() && testVar.ActionPoints >= minMovesLeft; i++ ) {
		if( isPosChange ) {
			const set<int>& addSet = GetCurrentState().GetVisibleSetForTrooper( testVar, false );
		//	result.insert( addSet.begin(), addSet.end() );
			result.push_back( &addSet );
			
			const set<int>& addSetSniper = GetCurrentState().GetVisibleSetForTrooper( testVar, true );
		//	resultSniper.insert( addSetSniper.begin(), addSetSniper.end() );
			resultSniper.push_back( &addSetSniper );
		}
		if( i >= currentTrooper.MovesLog.size() ) {
			break;
		}
		const CMove& move = currentTrooper.MovesLog[i];
		move.Apply( testVar );
		isPosChange = ( move.MoveType == MT_Move ) || ( move.MoveType == MT_Stand && move.Y > 0 );  
	} 
}

static int getDamageFromEnemy( TTrooperType trooperType, int stance, int usedMoves )
{
	const int actionPointsPerShoot = CMove::ActionPointsToMove( trooperType, MT_Shoot, 2 );
	const int actionPointsCount = 12; //trooperType == TT_Scout ? 12 : 10;
	const int shootDamage = CTrooperVariant::GetShootDamage( trooperType, stance ); 
	const int totalDamage = max( 0, ( (actionPointsCount - usedMoves ) / actionPointsPerShoot ) * shootDamage );
	return totalDamage;
}

double CExplorationTactic::getPenaltyForInvisibleEnemies( const CTrooperVariant& currentTrooper, const vector<const set<int>*>& visibleSet, const vector<const set<int>*>& visibleSetSniper ) const
{
	double result = 0;	
	CTrooperVariant shooter;
	const int myIndex = PointToIndex( currentTrooper.X, currentTrooper.Y, currentTrooper.Stance );
	const CEnemyProb& enemiesProb = GetCurrentState().EnemiesProb();
	for( int i = 0; i < enemiesProb.Size(); i++ ) {
		const TEnemyProbType enemyProbType = enemiesProb.GetEnemyProbType( i ); 
		if( enemyProbType == EPT_Visible ||	enemiesProb.GetEnemyProbSum( i ) == 0 ) {
			continue;
		}
		shooter.Type = static_cast<TTrooperType>( i % TT_Count );
		map<int, int>::const_iterator it = minMovesToShootForInvisibleEnemies[shooter.Type].find( myIndex );
		int bufferIndex = 0;
		if( it == minMovesToShootForInvisibleEnemies[shooter.Type].end() ) {
			minMovesToShootForInvisibleEnemies[shooter.Type][myIndex] = minMovesBuffer.size();
			bufferIndex = minMovesBuffer.size();
			minMovesBuffer.push_back( new CMinMovesToShoot( currentTrooper ) );
			minMovesBuffer.back()->BuildFromShooterToTrooper( 3, shooter, false );
		} else {
			bufferIndex = it->second;
		}
		double resultForEnemy = 0;
		const map<int, int>& minMoves = minMovesBuffer[bufferIndex]->GetMinMoves();
		const vector<const set<int>*>* currentVisibleSet = shooter.Type == TT_Sniper ? &visibleSetSniper : &visibleSet;
		
		int countOfPoints = 0;
		const set<int> currentProbs = enemiesProb.GetProbs( i );
		for( set<int>::const_iterator it = currentProbs.begin(); it != currentProbs.end(); it++ ) {
			if( !isVisibleSetHas( *currentVisibleSet, *it ) ) {
				countOfPoints++;
			}
		}
		
		for( map<int, int>::const_iterator it = minMoves.begin(); it != minMoves.end(); it++ ) {
			if( isVisibleSetHas( *currentVisibleSet, it->first ) ) {
				continue;
			}
			if( enemiesProb.GetEnemyProbCount( i, it->first ) == 1 ) {
				int x, y, stance;
				IndexToPoint( it->first, x, y, stance );
				if( ( shooter.Type == TT_Sniper) && ( GetCurrentState().GetDistanceEuk( x, y, currentTrooper.X, currentTrooper.Y ) > 8.5 ) ) {
//					continue;
				}
				stance = min( 2, stance + it->second );
				resultForEnemy += getDamageFromEnemy( shooter.Type, stance, 2 * it->second );
			}
		}
//		const double coefs = enemyProbType == EPT_FromDamage ? 1. : 0.5;
		const double coefs = 1;
		result += coefs * resultForEnemy / max( countOfPoints, 1 );
		//result += coefs * resultForEnemy / enemiesProb.GetEnemyProbSum( i );	
	}
	if( GetCurrentState().GameType() == GT_Five ) {
		if( GetCurrentState().TurnNumber >= 40 && GetMyScore() < GetEnemyScore() ) {
			result /= 9;
		} else if( GetCurrentState().TurnNumber >= 30 && GetMyScore() <= GetEnemyScore() ) {
			result /= 2;
		}
	}
	return result / 5;
}

bool CExplorationTactic::isVisibleSetHas( const vector<const set<int>* >& visibleSet, int point )
{
	for( int i = 0; i < visibleSet.size(); i++ ) {
		if( visibleSet[i]->find( point ) != visibleSet[i]->end() ) {
			return true;
		}
	}
	return false;
}

double CExplorationTactic::getExplorationBonus( const CTrooperVariant& currentTrooper ) const
{
	double result = 0;
	vector<const set<int>*> visibleSet;
	vector<const set<int>*> visibleSetSniper;
	buildVisibleSet( visibleSet, visibleSetSniper, currentTrooper, 0 );
	double bonusForSeeEnemy = 0.4;
	const CEnemyProb& enemyProb = GetCurrentState().EnemiesProb();
	for( int i = 0; i < enemyProb.Size(); i++ ) {
		const vector<const set<int>*> *currentVisibleSet = (i % TT_Count == TT_Sniper ) ? &visibleSetSniper : &visibleSet;
		const set<int> currentProbs = enemyProb.GetProbs( i );
		if( currentProbs.size() == 0 ) {
			continue;
		}
		int count = 0;	
		for( set<int>::const_iterator it = currentProbs.begin(); it != currentProbs.end(); it++ ) {
			if( isVisibleSetHas( *currentVisibleSet, *it ) ) {
				count++;
			}
		}
		result += 1. * bonusForSeeEnemy * count / currentProbs.size();
	}
	return result;
}

////////////////////////////////////////////////////////////////////////////////
inline int distance( int x0, int y0, int x1, int y1 )
{
	return abs( x1 - x0 ) + abs( y1 - y0 );
}

void CCheckpoints::Initialize()
{
	const CCurrentState curState = GetCurrentState();
	const int width = curState.GetWidth();
	const int height = curState.GetHeight();
	const int xCenter = width / 2;
	const int yCenter = height / 2;
	
	int bestX = curState.GetCurrentTrooper().X;
	int bestY = curState.GetCurrentTrooper().Y;
	int bestDist = distance( bestX, bestY, xCenter, yCenter );
	for( int i =0; i < curState.GetMyTroopers().size(); i++ ) {
		const int x = curState.GetMyTroopers()[i].X;
		const int y = curState.GetMyTroopers()[i].Y;
		const int dist = distance( x, y, xCenter, yCenter );
		if( dist < bestDist ) {
			bestX = x;
			bestY = y;
			bestDist = dist;
		}
	}

	const int deltaX = min( bestX, width - 1 - bestX );
	const int deltaY = min( bestY, height - 1 - bestY );
	
	checkpoints.clear();
	checkpoints.push_back( make_pair( deltaX, deltaY ) );
	checkpoints.push_back( make_pair( width - 1 - deltaX, deltaY ) );
	checkpoints.push_back( make_pair( width - 1 - deltaX, height - 1 - deltaY ) );
	checkpoints.push_back( make_pair( deltaX, height - 1 - deltaY ) );

	for( int i = 0; i < checkpoints.size(); i++ ) {
		if( checkpoints[i].first == bestX && checkpoints[i].second == bestY ) {
			curTarget = i;
			break;
		}
	}
	//Select direction ( clockwise or counterwise ) so, that first path was shortest:
	if( curTarget == 0 || curTarget == 2 ) {
		direction = -1;
	} else {
		direction = 1;
	}
	SetNextTarget();
	
}


CCheckpoints& GetCheckpoints() {
	static CCheckpoints checkpoints;
	return checkpoints;
}
