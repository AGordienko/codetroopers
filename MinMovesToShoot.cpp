#include "MinMovesToShoot.h"

CMinMovesToShoot::CMinMovesToShoot( const CTrooperVariant& _trooper ) :
	trooper( _trooper )
{
}

CMinMovesToShoot::CMinMovesToShoot( const CMinMovesToShoot& other ) :
	trooper( other.trooper ),
	minMoves( other.minMoves )
{
}

const int dx[] = { 1, 0, -1, 0 };
const int dy[] = { 0, 1, 0, -1 };

void CMinMovesToShoot::BuildFromTrooper( int depth, bool visibleInsteadShoot )
{
	vector<int> firstPositionSet;
	vector<int> secondPositionSet;
	vector<int>* first = &firstPositionSet;
	vector<int>* second = &secondPositionSet;

	const int width = GetCurrentState().GetWidth();
	const int height = GetCurrentState().GetHeight();
	set<int> processedPoints;

	const int shootRange = trooper.GetShootRange();
	const double visibleRange = trooper.GetVisibleRange( 0 );
	int x = trooper.X;
	int y = trooper.Y;
	int stance = trooper.Stance;
	const int index = PointToIndex( x, y , stance );
	first->push_back( index );
	processedPoints.insert( index );
	minMoves.clear();
	for( int i = 0; i <= depth; i++ ) {
		second->clear();
		for( int j = 0; j < first->size(); j++ ) {
			IndexToPoint( (*first)[j], x, y, stance );
			//Add all reachble points:
			for( int nx = max( 0, x - shootRange); nx <= min( width - 1, x + shootRange ); nx++ ) {
				for( int ny = max( 0, y - shootRange ); ny <= min( height - 1, y + shootRange ); ny++ ) {
					if( !GetCurrentState().IsCellFree( nx, ny ) ) {
						continue;
					}
					for( int nStance = 2; nStance >= 0; nStance-- ) {
						if( visibleInsteadShoot ) {
							bool isReachble = GetCurrentState().world->isVisible( visibleRange, x, y, CTrooperVariant::ConvertStance( stance ),
									nx, ny, CTrooperVariant::ConvertStance( nStance ) );
							if( !isReachble ) {
								break;
							}
							const int newIndex = PointToIndex( nx, ny, nStance );
							if( minMoves.find( newIndex ) == minMoves.end() ) {
								minMoves[newIndex] = i;
							}

							const double visibleRangeForSniper = visibleRange - ( 2 - nStance ) * 0.5;
							isReachble = GetCurrentState().world->isVisible( visibleRangeForSniper, x, y, CTrooperVariant::ConvertStance( stance ),
									nx, ny, CTrooperVariant::ConvertStance( nStance ) );
							if( minMovesToSeeSniper.find( newIndex ) == minMovesToSeeSniper.end() ) {
								minMovesToSeeSniper[newIndex] = i;
							}

						} else {
							bool isReachble = GetCurrentState().world->isVisible( shootRange, x, y, CTrooperVariant::ConvertStance( stance ),
									nx, ny, CTrooperVariant::ConvertStance( nStance ) );
							if( !isReachble ) {
								break;
							}
							const int newIndex = PointToIndex( nx, ny, nStance );
							if( minMoves.find( newIndex ) == minMoves.end() ) {
								minMoves[newIndex] = i;
							}
						}
					}
				}
			}
			
			//Make moves:
			if( stance < 2 ) {
				second->push_back( PointToIndex( x, y, stance + 1 ) );
			} else {
				for( int k = 0; k < 4; k++ ) {
					const int nx = x + dx[k];
					const int ny = y + dy[k];
					const int nindex = PointToIndex( nx, ny, stance );
					if( nx >= 0 && nx < width && ny >= 0 && ny < height && GetCurrentState().IsCellFree( nx, ny ) &&
						( processedPoints.find( nindex ) == processedPoints.end() ) )
					{
						processedPoints.insert( nindex );
						second->push_back( nindex );
					}
				}
			}
		}
		swap( first, second );
	}
}

void CMinMovesToShoot::BuildFromShooterToTrooper( int depth, CTrooperVariant shooter, bool visibleInsteadShoot )
{
	vector<int> firstPositionSet;
	vector<int> secondPositionSet;
	vector<int>* first = &firstPositionSet;
	vector<int>* second = &secondPositionSet;
	
	const int width = GetCurrentState().GetWidth();
	const int height = GetCurrentState().GetHeight();
	
	int x = trooper.X;
	int y = trooper.Y;

	minMoves.clear();
	//Add cells from which can shoot
	for( int nStance = 2; nStance >= 0; nStance-- ) {
		shooter.Stance = nStance;
		const int shootRange = shooter.GetShootRange();
		for( int nx = max( 0, x - shootRange ); nx <= min( width - 1, x + shootRange ); nx ++ ) {
			for( int ny = max( 0, y - shootRange ); ny <= min( height - 1, y + shootRange ); ny++ ) {
				if( !GetCurrentState().IsCellFree( nx, ny ) ) {
					continue;
				}
				shooter.X = nx;
				shooter.Y = ny;
				shooter.Stance = nStance;
				const int index = PointToIndex( nx, ny, nStance );
				if( minMoves.find( index ) != minMoves.end() ) {
					continue;
				}
				if( visibleInsteadShoot ) {
					bool isReachble = GetCurrentState().IsEnemyVisible( shooter, trooper );
					if( isReachble ) {
						minMoves[index] = 0;
						first->push_back( index );
						if( trooper.Type == TT_Sniper ) {
							minMovesToSeeSniper[index] = 0;
						}
					}
				} else {
					bool isReachble = GetCurrentState().CanShootToEnemy( shooter, trooper );
					if( isReachble ) {
						minMoves[index] = 0;
						first->push_back( index );
					}
				}
			}
		}
	}

	for( int i = 0; i < depth; i++ ) {
		second->clear();
		for( int j = 0; j < first->size(); j++ ) {
			int x;
			int y;
			int stance;
			IndexToPoint( (*first)[j], x, y, stance );
			//Try change stance:
			for( int nStance = max( 0, stance - 1 ); nStance <= min( 2, stance + 1 ); nStance++ ) {
				if( nStance == stance ) {
					continue;
				}
				shooter.X = x;
				shooter.Y = y;
				shooter.Stance = nStance;
				const int index = PointToIndex( x, y, nStance );
				if( minMoves.find( index ) != minMoves.end() ) {
					continue;
				}
				//cout << "SHOOT FROM: [ " << shooter.X << ", " << shooter.Y << ", " << shooter.Stance << " = " << i + 1 << endl;
				minMoves[index] = i + 1;
				if( trooper.Type == TT_Sniper ) {
					minMovesToSeeSniper[index] = i + 1;
				}
				second->push_back( index );
			}
			//Try to make one step from standing pos:
			if( stance != 2 ) {
				continue;
			}
			for( int k = 0; k < 4; k++ ) {
				const int nx = x + dx[k];
				const int ny = y + dy[k];
				if( nx < 0 || nx >= width || ny < 0 || ny >= height || !GetCurrentState().IsCellFree( nx, ny ) ) {
					continue;
				}
				shooter.X = nx;
				shooter.Y = ny;
				shooter.Stance = stance;
				const int index = PointToIndex( nx, ny, stance );
				if( minMoves.find( index ) != minMoves.end() ) {
					continue;
				}
				minMoves[index] = i + 1;
				if( trooper.Type == TT_Sniper ) {
					minMovesToSeeSniper[index] = i + 1;
				}
				second->push_back( index );
			}
		}
		swap( first, second );
	}

}
