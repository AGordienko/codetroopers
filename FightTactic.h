#pragma once
#ifndef FIGHTTACTIC_H
#define FIGHTTACTIC_H

#include "common.h"
#include "Tactic.h"
#include "MinMovesToShoot.h"

class CFightTactic : public ITactic {
public:
	virtual bool IsMoveUseless( const CMove& move ) const;
	virtual double GetPositionQuality( const CTrooperVariant& trooperVariants, bool dumpLog, double bestQuality = TacticWorstQuality ) const;
	virtual void Initialize();

	virtual ~CFightTactic();

private:
	vector<bool> isTrooperInFight;
	
	vector<CMinMovesToShoot> movesToShootToEnemies;
	vector<CMinMovesToShoot> movesToShootFromEnemies;
	vector<CMinMovesToShoot> movesToSeeMeFromEnemies;
	vector<map<int, int> > enemyDamageFromGrenade;
	mutable vector< map<int, int> > minMovesToShootForInvisibleEnemies;
	mutable vector<CMinMovesToShoot*> minMovesBuffer;

	vector<int> isFriendsVisible;
	vector<int> minMovesToShootToFriends;
		
	vector<long long> enemiesId;
	vector< vector<int> > enemiesMoveOrder;
	vector< vector<int> > enemiesIndexes;
	vector<int> maxDamageFromEnemy;
	
	map<int, double> probForShoot;

	int medicIndex;
	bool isEnemyCanShootToMyTroopers;
	bool isEnemyCanSeeMyTroopers;

	int friendsWounds;
	int countOfFriendsVisibleForEnemies;
	int countOfFriendsThatEnemiesCanShoot;

	int width;
	int height;
	
	int commanderIndex;
	mutable int minMovesToSniperSeeMe;

	double getPenaltyForInvisibleEnemies( const CTrooperVariant& currentTrooper, const vector<const set<int>*>& visibleSet, const vector<const set<int>*>& visibleSetSniper ) const;
//	void buildVisibleSet( set<int>& result, set<int>& sniper, const CTrooperVariant& currentTrooper, int minMovesLeft ) const;
	void buildVisibleSet( vector<const set<int>*>& result, vector< const set<int>*>& resultSniper, const CTrooperVariant& currentTrooper, int minMovesLeft ) const;
	
	double getSpecialTrooperBonus( const CTrooperVariant& currentTrooper ) const;
	
	int getPositionPenaltyNew( const CTrooperVariant& trooperVariant ) const;
	int getPositionPenalty( const CTrooperVariant& trooperVariant ) const;
	double getExplorationBonus( const CTrooperVariant& currentTrooper ) const;
	static bool isVisibleSetHas( const vector<const set<int>* >& visibleSet, int point );

	void calcProbForShoot();
};

#endif //FIGHTTACTIC_H
