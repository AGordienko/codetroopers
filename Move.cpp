#include "Move.h"
#include "CurrentState.h"

vector< vector< int> > CMove::neededActionPoints;
vector<int> CMove::neededActionPointsForMove;

void CMove::CalculateeNeededActionPoints()
{
	const int actionPointsForImpossibleMove = 1000;
	neededActionPoints = vector< vector<int> >(TT_Count, vector<int>( MT_Count, actionPointsForImpossibleMove ) );
	neededActionPointsForMove.clear();
	neededActionPointsForMove.push_back( 6 );
	neededActionPointsForMove.push_back( 4 );
	neededActionPointsForMove.push_back( 2 );
	for( int j = 0; j < MT_Count; j++ ) {
		switch( j ) {
			case MT_Shoot :
				for( int i = 0; i < TT_Count; i++ ) {
					switch( i ) {
						case TT_Commander :
							neededActionPoints[i][j] = 3;
							break;
						case TT_Medic :
							neededActionPoints[i][j] = 2;
							break;
						case TT_Soldier :
							neededActionPoints[i][j] = 4;
							break;
						case TT_Sniper :
							neededActionPoints[i][j] = 9;
							break;
						case TT_Scout :
							neededActionPoints[i][j] = 4;
							break;
							
					}
				}
				break;

			case MT_Stand : 
				for( int i = 0; i < TT_Count; i++ ) {
					neededActionPoints[i][j] = 2;
				}
				break;

			case MT_Skip : 
				for( int i = 0; i < TT_Count; i++ ) {
					neededActionPoints[i][j] = 0;
				}
				break;
			
			case MT_UseMedikt : 
				for( int i = 0; i < TT_Count; i++ ) {
					neededActionPoints[i][j] = 2;
				}
				break;
			
			case MT_UseGrenade : 
				for( int i = 0; i < TT_Count; i++ ) {
					neededActionPoints[i][j] = 8;
				}
				break;
			
			case MT_UseRation : 
				for( int i = 0; i < TT_Count; i++ ) {
					neededActionPoints[i][j] = 2;
				}
				break;

			case MT_Health : 
				for( int i = 0; i < TT_Count; i++ ) {
					neededActionPoints[i][j] = actionPointsForImpossibleMove;
				}
				neededActionPoints[TT_Medic][j] = 1;
				break;
			
			case MT_GetRadarInfo : 
				for( int i = 0; i < TT_Count; i++ ) {
					neededActionPoints[i][j] = actionPointsForImpossibleMove;
				}
				neededActionPoints[TT_Commander][j] = 10;
				break;
		}
	}
}

CMove::CMove( TMoveType _moveType, int _x, int _y) :
	MoveType( _moveType ),
	X( _x ),
	Y( _y )
{
}

CMove::CMove( const CMove& other ) :
	MoveType( other.MoveType ),
	X( other.X ),
	Y( other.Y )
{
}

bool CMove::Apply( CTrooperVariant& trooperVariant ) const
{
	const int actionPointsPerMove = ActionPointsToMove( trooperVariant.Type, MoveType, trooperVariant.Stance );
	if( trooperVariant.ActionPoints < actionPointsPerMove ) {
		return false;
	}
	const vector<CTrooperVariant>& myTroopers = GetCurrentState().GetMyTroopers();
	const vector<CTrooperVariant>& enemyTroopers = GetCurrentState().GetEnemyTroopers();
	
	int nx = trooperVariant.X + X;
	int ny = trooperVariant.Y + Y;
	int shootDamage;
	int shootRange;
	int targetX;
	int targetY;
	int centerEnemyIndex;
	const double eps = 1e-8;
	double dist;
	const set<int>* visibleSet;

	int result = false;
	switch( MoveType ) {
		case MT_Move :
			if( nx < 0 || nx >= GetCurrentState().GetWidth() || ny < 0 || ny >= GetCurrentState().GetHeight() ) {
				return false;
			}
			if( GetCurrentState().IsCellFree( nx, ny ) ) {
				trooperVariant.X = nx;
				trooperVariant.Y = ny;
				const TCellInfo cellInfo = GetCurrentState().GetCellInfo( nx, ny );
				visibleSet = &GetCurrentState().GetVisibleSetForTrooper( trooperVariant, false );
//				trooperVariant.VisibleCells.insert( visibleSet->begin(), visibleSet->end() );
				if( HasCellBonus( cellInfo ) && !trooperVariant.IsBonusUsed( nx, ny ) ) {
					if( !trooperVariant.HasMedikt &&  ( cellInfo & CI_Medict ) == CI_Medict ) {
						trooperVariant.HasMedikt = true;
						trooperVariant.AddBonusToUsed( nx, ny );
					} else if( !trooperVariant.HasGrenade &&  ( cellInfo & CI_Grenage ) == CI_Grenage ) {
						trooperVariant.HasGrenade = true;
						trooperVariant.AddBonusToUsed( nx, ny );
					} else if( !trooperVariant.HasRation &&  ( cellInfo & CI_Ration ) == CI_Ration ) {
						trooperVariant.HasRation = true;
						trooperVariant.AddBonusToUsed( nx, ny );
					}
				}
				result = true;
			}
			break;
		case MT_Stand :
			if( trooperVariant.Stance + Y < 0 ||
				trooperVariant.Stance + Y > 2 )
			{
				return false;
			}
			trooperVariant.Stance += Y;
			if( Y > 0 ) {
				visibleSet = &GetCurrentState().GetVisibleSetForTrooper( trooperVariant, false );
//				trooperVariant.VisibleCells.insert( visibleSet->begin(), visibleSet->end() );
			}
			result = true;
			break;
		case MT_UseMedikt :
			if( !trooperVariant.HasMedikt ) {
				return false;
			}
			if( Y == -1 ) {
				if( trooperVariant.Health >= 100 || trooperVariant.Health <= 0 ) {
					return false;
				}
				trooperVariant.Health = min( 100, trooperVariant.Health + 30 );
			} else {
				if( trooperVariant.FriendsHealth[Y] >= 100 || trooperVariant.FriendsHealth[Y] <= 0 ||
					( abs( myTroopers[Y].Y - trooperVariant.Y ) + abs( myTroopers[Y].X - trooperVariant.X ) ) != 1 )
				{
					return false;
				}
				trooperVariant.FriendsHealth[Y] = min( 100, 50 + trooperVariant.FriendsHealth[Y] );
			}
			trooperVariant.HasMedikt = false;
			if( GetCurrentState().GetCellInfo( trooperVariant.X, trooperVariant.Y ) == CI_Medict &&
				!trooperVariant.IsBonusUsed( trooperVariant.X, trooperVariant.Y ) )
			{
				trooperVariant.HasMedikt = true;
				trooperVariant.AddBonusToUsed( trooperVariant.X, trooperVariant.Y );
			}
			result = true;
			break;
		case MT_UseGrenade :
			targetX = Y >> 8;
			targetY = Y & ( ( 1 << 8 ) - 1);
			centerEnemyIndex = (X >> 16) - 1;
			dist = GetCurrentState().GetDistanceEuk( trooperVariant.X, trooperVariant.Y, targetX, targetY ); 
			if( !trooperVariant.HasGrenade || dist <= 1. + eps || dist > 5 + eps ) {
				return false;
			}
			if( centerEnemyIndex != -1 ) {
				trooperVariant.EnemiesHealth[centerEnemyIndex] = max( 0, trooperVariant.EnemiesHealth[centerEnemyIndex] - 80 );
			}
			for( int i = 0; i < enemyTroopers.size(); i++ ) {
				if( X & ( 1 << i ) ) {
					trooperVariant.EnemiesHealth[i] = max( 0, trooperVariant.EnemiesHealth[i] - 60 );
				}
			}
			trooperVariant.HasGrenade = false;
			if( GetCurrentState().GetCellInfo( trooperVariant.X, trooperVariant.Y ) == CI_Grenage &&
				!trooperVariant.IsBonusUsed( trooperVariant.X, trooperVariant.Y ) )
			{
				trooperVariant.HasGrenade = true;
				trooperVariant.AddBonusToUsed( trooperVariant.X, trooperVariant.Y );
			}
			result = true;
			break;
		case MT_UseRation :
			if( !trooperVariant.HasRation || trooperVariant.ActionPoints > 8 ) {
				return false;
			}
			trooperVariant.HasRation= false;
			if( GetCurrentState().GetCellInfo( trooperVariant.X, trooperVariant.Y ) == CI_Ration &&
				!trooperVariant.IsBonusUsed( trooperVariant.X, trooperVariant.Y ) )
			{
				trooperVariant.HasRation = true;
				trooperVariant.AddBonusToUsed( trooperVariant.X, trooperVariant.Y );
			}
			trooperVariant.ActionPoints = min( trooperVariant.ActionPoints + 5, trooperVariant.GetMaxActionPoints() + 2 );
			result = true;
			break;
		case MT_Health :
			if( trooperVariant.Type != TT_Medic ) {
				return false;
			}
			if( Y == -1 ) {
				if( trooperVariant.Health >= 100 ) {
					return false;
				}
				trooperVariant.Health = min( 100, trooperVariant.Health + 3 );
			} else {
				if( trooperVariant.FriendsHealth[Y] >= 100 || trooperVariant.FriendsHealth[Y] <= 0 ||
					( abs( myTroopers[Y].Y - trooperVariant.Y ) + abs( myTroopers[Y].X - trooperVariant.X ) ) != 1 )
				{
					return false;
				}
				trooperVariant.FriendsHealth[Y] = min( 100, 5 + trooperVariant.FriendsHealth[Y] );
			}
			result = true;
			break;
		case MT_Shoot :
			shootRange = trooperVariant.GetShootRange();
			if( Y == -1 ) {
				if( !trooperVariant.TryRandomShoot ) {
					trooperVariant.TryRandomShoot = true;
					result = true;
				} else {
					return false;
				}
				break;
			}
			if( !GetCurrentState().world->isVisible( shootRange, trooperVariant.X, trooperVariant.Y, trooperVariant.ConvertStance(), enemyTroopers[Y].X, enemyTroopers[Y].Y, enemyTroopers[Y].ConvertStance() ) ) {
				return false;
			}
			shootDamage = trooperVariant.GetShootDamage();
			trooperVariant.EnemiesHealth[Y] = max( 0, trooperVariant.EnemiesHealth[Y] - shootDamage );
			result = true;
			break;

		case MT_GetRadarInfo :
			result = true;
			break;
		case MT_Skip :
			result = true;
			break;
	}
	if( !result ) {
		return false;
	}

	trooperVariant.ActionPoints -= actionPointsPerMove;
	
	return true;
}

void CMove::MakeMove( Move& gameMove ) const
{
	const CTrooperVariant& curTrooper = GetCurrentState().GetCurrentTrooper();
	const vector<CTrooperVariant>& myTroopers = GetCurrentState().GetMyTroopers();
	const vector<CTrooperVariant>& enemyTroopers = GetCurrentState().GetEnemyTroopers();
	
	int nx = curTrooper.X + X;
	int ny = curTrooper.Y + Y;
	int nStance = 0;
	int targetX;
	int targetY;
	switch( MoveType ) {
		case MT_Move :
			gameMove.setAction( MOVE );
			gameMove.setX( nx );
			gameMove.setY( ny );
			break;
		case MT_Shoot :
			if( Y == -1 ) {
				IndexToPoint( X, nx, ny, nStance );
				gameMove.setAction( SHOOT );
				gameMove.setX( nx );
				gameMove.setY( ny );
				GetCurrentState().RandomShoot = X;
				GetCurrentState().LastTurnShootDamage = curTrooper.GetShootDamage();
				if( DumpLog ) {
					cout << "MOVE RANDOM SHOOT: " << nx << ", " <<  ny << "(" << GetCurrentState().RandomShoot << ")" << endl;
				}
				assert( X != -1 );
				break;
			}
			gameMove.setAction( SHOOT );
			gameMove.setX( enemyTroopers[Y].X );
			gameMove.setY( enemyTroopers[Y].Y );
			GetCurrentState().LastTurnShootDamage = curTrooper.GetShootDamage();
			GetCurrentState().LastTurnShootToEnemy = enemyTroopers[Y].Type;	
			GetCurrentState().LastTurnShootToEnemyId = enemyTroopers[Y].PlayerId;
			break;
		case MT_Stand :
			gameMove.setAction( Y > 0 ? RAISE_STANCE : LOWER_STANCE );
			break;
		case MT_UseMedikt :
			gameMove.setAction( USE_MEDIKIT );
			if( Y == -1 ) {
				gameMove.setX( curTrooper.X );
				gameMove.setY( curTrooper.Y );
			} else {
				gameMove.setX( myTroopers[Y].X );
				gameMove.setY( myTroopers[Y].Y );
			}
			break;
		case MT_UseGrenade :
			gameMove.setAction( THROW_GRENADE );
			targetX = Y >> 8;
			targetY = Y & ( ( 1 << 8 ) - 1);
			gameMove.setX( targetX );
			gameMove.setY( targetY );
			break;
		case MT_UseRation :
			gameMove.setAction( EAT_FIELD_RATION );
			break;
		case MT_Health :
			gameMove.setAction( HEAL );
			if( Y == -1 ) {
				gameMove.setX( curTrooper.X );
				gameMove.setY( curTrooper.Y );
			} else {
				gameMove.setX( myTroopers[Y].X );
				gameMove.setY( myTroopers[Y].Y );
			}
			break;
		case MT_GetRadarInfo :
			gameMove.setAction( REQUEST_ENEMY_DISPOSITION );
			break;
		case MT_Skip :
			gameMove.setAction( END_TURN );
			break;
		default:
			assert( false );
	}
	
	GetCurrentState().SaveEnemyTroopers( *this );
	GetCurrentState().PredTrooperType = curTrooper.Type;
}

static const int dxRange[] = { 1, 0, -1, 0 };
static const int dyRange[] = { 0, 1, 0, -1 };

void CMove::GetAllMoves( vector<CMove>& moves )
{
	const CTrooperVariant& currentTrooper = GetCurrentState().GetCurrentTrooper();
	const TTrooperType currentTrooperType = currentTrooper.Type;
	const vector<CTrooperVariant>& myTroopers = GetCurrentState().GetMyTroopers();
	const vector<CTrooperVariant>& enemyTroopers = GetCurrentState().GetEnemyTroopers();

	moves.clear();
	
//	moves.push_back( CMove( MT_Skip, 0, 0 ) );
	
	if( currentTrooperType == TT_Medic ) {
		if( currentTrooper.Health < 100 ) {
			moves.push_back( CMove( MT_Health, -1, -1 ) );
		}
		for( int i = 0; i < myTroopers.size(); i++ ) {
			if( myTroopers[i].Health < 100 ) {
				moves.push_back( CMove( MT_Health, -1, i ) );
			}	
		}
	}
	
	moves.push_back( CMove( MT_UseRation, -1, -1 ) );

	if( currentTrooper.Health < 100 ) {
		moves.push_back( CMove( MT_UseMedikt, -1, -1 ) );
	}
	for( int i = 0; i < myTroopers.size(); i++ ) {
		if( myTroopers[i].Health < 100 ) {
			moves.push_back( CMove( MT_UseMedikt, -1, i ) );
		}	
	}
	

	for( int i =0; i < enemyTroopers.size(); i++ ) {
		moves.push_back( CMove( MT_Shoot, -1, i ) );
	}
	moves.push_back( CMove( MT_Shoot, -1, -1 ) );
		
	set<int> targets;
	for( int k = -1; k < 4; k++ ) {
		for( int i =0; i < enemyTroopers.size(); i++ ) {
			const int targetX = enemyTroopers[i].X + ( k == -1 ? 0 : dxRange[k] );
			const int targetY = enemyTroopers[i].Y + ( k == -1 ? 0 : dyRange[k] );
			if( targetX < 0 || targetX >= GetCurrentState().GetWidth() || targetY < 0 || targetY >= GetCurrentState().GetHeight() ) {
				continue;
			}
			const int targetIndex = ( targetX << 8 ) | targetY;
			if( targets.find( targetIndex ) != targets.end() ) {
				continue;
			}
			targets.insert( targetIndex );
			bool isDangerForMyTrooper = false;
			for( int j = 0; j < myTroopers.size(); j++ ) {
				if( abs( targetX - myTroopers[j].X ) + abs( targetY - myTroopers[j].Y ) == 1 ) {
					isDangerForMyTrooper = true;
					break;
				}
			}
			if( isDangerForMyTrooper ) {
				continue;
			}
			int maskForSib = 0;
			if( k == -1 ) {
				maskForSib |= ( ( i + 1 ) << 16 );
			}
			for( int j = 0; j < enemyTroopers.size(); j++ ) {
				if( abs( targetX - enemyTroopers[j].X ) + abs( targetY - enemyTroopers[j].Y ) == 1 ) {
					maskForSib = maskForSib | ( 1 << j );
				}
			}
			moves.push_back( CMove( MT_UseGrenade, maskForSib, targetIndex ) );
		}
	}
	
	moves.push_back( CMove( MT_Stand, 0, 1 ) );
	moves.push_back( CMove( MT_Stand, 0, -1 ) );
	
	for( int i = 0; i < 4; i++ ) {
		moves.push_back( CMove( MT_Move, dxRange[i], dyRange[i] ) );
	}

	moves.push_back( CMove( MT_GetRadarInfo, -1, -1 ) );
}

void CMove::Log() const
{
	switch( MoveType ) {
		case MT_Move : 
			cout << "Move";
			break;
		case MT_Shoot :
			cout << "Shoot";
			break;
		case MT_Stand :
			cout << "Stand";
			break;
		case MT_Skip :
			cout << "Skip";
			break;
		case MT_UseMedikt :
			cout << "UseMedikt";
			break;
		case MT_UseGrenade:
			cout << "UseGrenade";
			break;
		case MT_UseRation :
			cout << "UseRation";
			break;
		case MT_Health:
			cout << "Health";
			break;
		case MT_GetRadarInfo:
			cout << "Radar";
			break;
	}
	cout << " (" << X << ", " << Y << ")";
}

