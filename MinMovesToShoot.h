#pragma once
#ifndef MINMOVOVESTOSHOOT_H
#define MINMOVOVESTOSHOOT_H

#include "common.h"
#include "CurrentState.h"

class CMinMovesToShoot {
public:
	CMinMovesToShoot( const CTrooperVariant& _trooper );
	CMinMovesToShoot( const CMinMovesToShoot& other );

	void BuildFromTrooper( int depth, bool visibleInsteadShoot );
	void BuildFromShooterToTrooper( int depth, CTrooperVariant shooter, bool visibleInsteadShoot );
	int Get( int x, int y, int stance ) const; 
	const map<int, int>& GetMinMoves() const { return minMoves; }
	int GetMinMovesToSeeTrooper( TTrooperType trooperType, TTrooperType targetTrooper, int x, int y, int stance ) const;

private:
	CTrooperVariant trooper;
	map<int, int> minMoves;
	map<int, int> minMovesToSeeSniper;
};

inline int CMinMovesToShoot::Get( int x, int y, int stance ) const 
{
	const map<int, int>::const_iterator it = minMoves.find( PointToIndex( x, y, stance ) );
	return it == minMoves.end() ? NonReachable : it->second;
}

inline int CMinMovesToShoot::GetMinMovesToSeeTrooper( TTrooperType trooperType, TTrooperType targetTrooper, int x, int y, int stance ) const 
{
	if( trooperType != TT_Scout && targetTrooper == TT_Sniper ) {
		const map<int, int>::const_iterator it = minMovesToSeeSniper.find( PointToIndex( x, y, stance ) );
		return it == minMoves.end() ? NonReachable : it->second;

	} else {
		const map<int, int>::const_iterator it = minMoves.find( PointToIndex( x, y, stance ) );
		return it == minMoves.end() ? NonReachable : it->second;
	}
}

#endif //MINMOVOVESTOSHOOT_H
